#!/bin/bash
# This file should be located in openssl/src
# Installed versions should be located in openssl/version
#
# If no arguments are given then this script searches for 
# OpenSSL tar.gz archives and installs them
# Options are:
# -f : Force install. Does not ask for user confirmation
# -v : Specify a version. Will download archive if not
#      present and then install it. Several -v can be used.


set -e # Exit on first error

# Determine if current user has root access
# Needed to create dir in $BASE_DIR (in make install_sw)
source is_root_user.sh
is_root_user

BASE_DIR=$(cd `dirname .` && pwd)
BASE_DIR=$(readlink -f ${BASE_DIR}/..)
SOURCE_OLD_URL=https://www.openssl.org/source/old
SOURCE_URL=https://www.openssl.org/source


# Helpful for testing
check_for_error() {
if [ $? -eq 0 ]; then
   echo "**** $1 OK ****"
else
  echo "**** $1 FAIL ****"
  exit 1
fi
}

# Download URL varies between latest and old releases
# This function tests and returns the desired one
get_url() {
  version=$1
  major=${version//[!0-9.]/}
  url=${SOURCE_OLD_URL}/${major}/openssl-${version}.tar.gz
  resp=$(curl -s --head ${url} | head -n 1 | grep "HTTP/1.[01] [23]..")
  if [ $? -eq 0 ] ; then
    echo ${url}
  else
    # Version is the newest one -> diff url
    url=${SOURCE_URL}/openssl-${version}.tar.gz
    resp=$(curl -s --head ${url} | head -n 1 | grep "HTTP/1.[01] [23]..")
    if [ $? -eq 0 ] ; then
      echo ${url}
    else
      echo "error"
      exit
    fi
  fi
}

# Downloads the given version if needed
download_archive() {
  version=$1
  archive=$2
  url=$(get_url $version)
  if [ $url = "error" ]; then
    echo "No valid URL found for version ${version}"
    exit
  fi
  if ! [ -f ${archive} ]; then
    echo "Downloading ${archive}..."
    wget $url
  else
    echo "${archive} already downloaded"
  fi
}

# Extracts the given archive if needed
extract_archive() {
  dir=$1
  archive=$2
  if ! [ -d ${dir} ]; then
    echo "Extracting ${archive} ..."
    tar -zxvf ${archive}
    check_for_error "tar"
   else
   echo "${archive} already extracted"
  fi
}

# Install given version
install_version() {
  version=$1
  dir=$2
  
  cd $BASE_DIR/src/${dir} # src/openssl-version
  install_dir=${BASE_DIR}/${version}
  mkdir ${install_dir}
  ./config --prefix=${install_dir}
  check_for_error "config"
  make
  check_for_error "make"
  make test
  check_for_error "make test"
  make install_sw
  check_for_error "make install_sw"
  cd ${BASE_DIR}/src
}

# Downlad, extract and install given version
install_server() {
  version=$1
  dir=openssl-$version
  archive=openssl-${version}.tar.gz 
  
  download_archive $version $archive 
  extract_archive $dir $archive  
  
  install_dir=${BASE_DIR}/${version}
  if [ -d ${install_dir} ]; then
    echo "${dir} already installed"
  else
	  echo "Installing ${version}"
    install_version ${version} ${dir}
  fi
}

# Installs the version if it is not already installed
install_if_new() {
x=$1
dir=${x%.tar.gz}
version=${dir#openssl-}
install_dir=${BASE_DIR}/${version}
if ! [ -d ${install_dir} ]; then
  install_server ${version}
fi
return 0
}

# Prints the version if it is not already installed
print_if_new() {
x=$1
dir=${x%.tar.gz}
version=${dir#openssl-}
install_dir=${BASE_DIR}/${version}
if ! [ -d ${install_dir} ]; then
	echo ${dir}
fi
return 0
}

# Parameter flags
FORCE_FLAG=false
VERSION_FLAG=false

# Handle input parameters
while getopts ":v:f" opt; do
  case $opt in
    f)
      FORCE_FLAG=true	
      ;;
    v)
      VERSION_FLAG=true
      install_server ${OPTARG,,}	
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

# Don't do look-up if version specified
if $VERSION_FLAG ; then
  exit
fi

# Ask for confirmation to install versions
if ! $FORCE_FLAG; then
  echo "The following versions will be installed:"
  find ${DIR} -maxdepth 1 -name "openssl-*.tar.gz" -type f -printf "%f\n" | while read file; do print_if_new "$file"; done
  echo "-------------------"
  echo "Do you wish to proceed? (Enter 1 or 2)"
  select yn in "Yes" "No"; do
      case $yn in
          Yes ) break;;
          No ) exit;;
      esac
  done
fi

# Install all archives found if not already installed
find ${DIR} -maxdepth 1 -name "openssl-*.tar.gz" -type f -printf "%f\n" | while read file; do install_if_new "$file"; done


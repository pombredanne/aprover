# Plugin makefile
# Please specify elsewhere the following before including this file
# TOOL_PATH path to the location of the main tool
# NACL_SDK_ROOT absoulte path to where the toolchain folder is
# TARGET = main_plugin
# TOOLCHAIN = glibc
# NACL_ARCH (x86_64 or x86_32)
# PLUGIN_PATH path to this folder

$(info $ --- Variables in plugin.mk)
$(info $ --- TOOL_PATH: $(TOOL_PATH))
$(info $ --- NACL_SDK_ROOT: $(NACL_SDK_ROOT))
$(info $ --- NACL_ARCH: $(NACL_ARCH))
$(info $ --- PLUGIN_PATH: $(PLUGIN_PATH))

#----------------------------
# Copy files to dst
# Create dst dir if not exists
# $(1) src
# $(2) dst
define COPY_RULE
$(shell mkdir -p $(dir $(2)))
$(shell cp $(1) $(2))
endef

# Sync dir
# $(1) src
# $(2) dst
define SYNC_RULE
$(shell mkdir -p $(dir $(2)))
$(shell rsync -rupE $(1) $(2))
endef

VALID_TOOLCHAINS := glibc

DEPS = nacl_io ppapi_cpp 
LIBS = nacl_io ppapi_cpp ppapi dl pthread crypto ssl

# plugin

# Paths to the different directories
INCL_DIR = $(TOOL_PATH)/incl
INCL_LIB_DIR = $(TOOL_PATH)/incl_lib
SRC_DIR = $(TOOL_PATH)/src
OBJ_DIR = $(TOOL_PATH)/obj
LIB_DIR = $(TOOL_PATH)/lib
LIB_SRC = $(TOOL_PATH)/lib_src

# Search for header files in $(INCL_DIR) add INCL_LIB_DIR as system header (now warnings)
FLAGS += -isystem $(INCL_LIB_DIR)
FLAGS += -I $(INCL_DIR)

# rdynamic to load symbols neeede by shared libs
NACL_LDFLAGS += -rdynamic

# Used to compile sqlite3
cc = gcc

#LIBS_FLAGS = -lssl -lcrypto -ldl 
FLAGS += -Wall -Wformat-security -Wno-deprecated-declarations -Wno-unused-function
FLAGS += -fPIC
CFLAGS += $(FLAGS) 
CXXFLAGS += $(FLAGS) 
# C++11
CXXFLAGS += -std=c++0x  -DNDEBUG
# C99
CFLAGS += -D_POSIX_SOURCE -std=c99 -O3

# The .cpp and .o files used for the main target
CPP_FILES := $(wildcard $(SRC_DIR)/*.cpp)

# Remove main.cpp and specify main_plugin target below instead
CPP_FILES := $(patsubst $(SRC_DIR)/main.cpp,,$(CPP_FILES))
C_FILES := $(wildcard $(SRC_DIR)/*.c)
OBJ_FILES := $(addprefix $(OBJ_DIR)/,$(notdir $(CPP_FILES:.cpp=.o)))
C_OBJ_FILES := $(addprefix $(OBJ_DIR)/,$(notdir $(C_FILES:.c=.o)))

# The .cpp files that represents the features and the resutling .so files
LIB_SRC_FILES := $(wildcard $(LIB_SRC)/*.cpp)
LIB_FILES := $(addprefix $(LIB_DIR)/,$(notdir $(LIB_SRC_FILES:.cpp=.so)))

# The .c files that represents the features and the resutling .cso files
C_LIB_SRC_FILES := $(wildcard $(LIB_SRC)/*.c)
C_LIB_FILES := $(addprefix $(LIB_DIR)/,$(notdir $(C_LIB_SRC_FILES:.c=.so)))


main_plugin_CFLAGS = $(CXXFLAGS)
main_plugin_SOURCES = $(PLUGIN_PATH)/main_plugin.cpp
main_plugin_SOURCES += $(CPP_FILES)

C_link_SOURCES = $(SRC_DIR)/c_probe_utils.c
C_link_SOURCES += $(SRC_DIR)/sqlite3.c

# Build rules
$(foreach dep,$(DEPS),$(eval $(call DEPEND_RULE,$(dep))))
$(foreach src,$(main_plugin_SOURCES),$(eval $(call COMPILE_RULE,$(src),$(main_plugin_CFLAGS))))
$(foreach src,$(SRC_DIR)/c_probe_utils.c,$(eval $(call COMPILE_RULE,$(src),$(CFLAGS))))
$(foreach src,$(SRC_DIR)/sqlite3.c,$(eval $(call COMPILE_RULE,$(src),$(CFLAGS))))
$(foreach src,$(LIB_SRC_FILES),$(eval $(call COMPILE_RULE,$(src),$(CXXFLAGS))))
$(foreach src,$(C_LIB_SRC_FILES),$(eval $(call COMPILE_RULE,$(src),$(CFLAGS))))

# Link main program
$(eval $(call LINK_RULE,$(TARGET),$(main_plugin_SOURCES),$(LIBS),$(DEPS)))
$(eval $(call LINK_RULE,$(TARGET),$(C_link_SOURCES),$(LIBS),$(DEPS)))

# Shared libraries
$(foreach lib_src,$(LIB_SRC_FILES),$(eval $(call MY_SO_RULE,$(patsubst $(LIB_SRC)/%.cpp,/%,$(lib_src)),$(lib_src), $(LIBS), $(DEPS))))
$(foreach c_lib_src,$(C_LIB_SRC_FILES),$(eval $(call MY_SO_RULE,$(patsubst $(LIB_SRC)/%.c,/%,$(c_lib_src)),$(c_lib_src), $(LIBS), $(DEPS))))

$(eval $(call NMF_RULE,$(TARGET),))

DB_NAME=aprover_web.db
DB_FILE=$(TOOL_PATH)/$(DB_NAME)

# Copy database file to the plugin release folder
ifneq ("$(wildcard $(DB_FILE))","")
$(eval $(call COPY_RULE,$(DB_FILE),glibc/Release/$(DB_NAME)))
$(info $ DB file: [${DB_FILE}] copied to Release)
else
$(info $ You can copy the database file manually to glibc/Release/$(DB_NAME) instead if you want)
$(error $ Please run the main tool to generate the database file: [${DB_FILE}])
endif

# Create tmp files used by sqlite
$(shell mkdir -p glibc/Release)
$(shell touch glibc/Release/$(DB_NAME)-wal)
$(shell touch glibc/Release/$(DB_NAME)-journal)

# Sync shared files
$(eval $(call SYNC_RULE,$(PLUGIN_PATH)/shared/,./))

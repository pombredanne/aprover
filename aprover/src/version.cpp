//
//  version.cpp
//  db
//
//  Created by Martin Andersson on 28/05/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Functions to comvert between the two OpenSSL version
 * representations, enum and string.
 */

#include "version.h"

using namespace std;

OPENSSL_VERSION version_str2enum(const std::string& s) {
  if (s.length() < 5 || s.length() > 7) {
    fprintf(stderr, "Error: version string wrong length,  s: %s\n", s.c_str());
    throw format_error("get_openssl_enum:: OpenSSL version string has the wrong format\n");
  }
  
  OPENSSL_VERSION e;
  std::vector<string>::const_iterator it;
  
  it = find(OPENSSL_VERSION_name.begin(), OPENSSL_VERSION_name.end(), s);
  if (it == OPENSSL_VERSION_name.end())
    e = OPENSSL_VERSION::UNKNOWN; // string not found
  else
    e = static_cast<OPENSSL_VERSION>(int(it - OPENSSL_VERSION_name.begin())); // get index
  return e;
}

const string version_enum2str(const OPENSSL_VERSION& e) {
  return OPENSSL_VERSION_name[static_cast<size_t>(e)];
}

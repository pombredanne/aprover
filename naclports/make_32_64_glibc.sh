#!/bin/sh
# This script builds the given naclports in both 32 and 64 bit 
# configuration with the glibc toolchain.

set -x
set -e

TARGETS="$*"
TARGETS=${TARGETS:-all}
BUILD_FLAGS=--ignore-disabled

export BUILD_FLAGS

NACL_SDK_ROOT=/Users/Martin/Documents/code/nacl_sdk/pepper_43
export NACL_SDK_ROOT

# x86_64 NaCl
NACL_ARCH=x86_64     TOOLCHAIN=glibc        make ${TARGETS}

# i686 NaCl
NACL_ARCH=i686       TOOLCHAIN=glibc        make ${TARGETS}

# Emscripten
if [ -n "${EMSCRIPTEN:-}" ]; then
  NACL_ARCH=emscripten TOOLCHAIN=emscripten   make ${TARGETS}
fi

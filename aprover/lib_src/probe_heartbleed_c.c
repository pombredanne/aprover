//
//  probe_heartbleed.c
//  aprover
//
//  Created by Martin Andersson on 08/07/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//


/*
 * Probes for the Heartbleed vulnerability, CVE-2014-0160
 *
 * Connection setup is based on the script from the OpenSSL wiki page
 * https://wiki.openssl.org/index.php/SSL/TLS_Client
 */

// Return codes
#define EXIT_HEARTBLEED    1
#define EXIT_NO_HEARTBLEED 0

#include "c_probe_utils.h"

// Probing technique using the OpenSSL C API
int c_heartbleed_probe(const char* host, int verbose)
{
  int ret = EXIT_NO_HEARTBLEED;
  if (verbose) printf("probe_heartbleed.c host in: %s\n", host);
  
  long res = 1;
  unsigned long ssl_err = 0;

  SSL_CTX* ctx = NULL;
  BIO *sbio = NULL;
  
  SSL *ssl = NULL;
  
  int socket = 0;
  struct SSL_record_info record;
  
  do {
    // Internal function that wraps the OpenSSL init's  
    init_openssl_library();
    
    const SSL_METHOD* method = SSLv23_method();
    ssl_err = ERR_get_error();
    assert(NULL != method);
    if(!(NULL != method)) {
      print_ssl_error_string(ssl_err, "SSLv23_method");
      break; // failed
    }
    
    ctx = SSL_CTX_new(method);
    ssl_err = ERR_get_error();
    assert(ctx != NULL);
    if(!(ctx != NULL)) {
      print_ssl_error_string(ssl_err, "SSL_CTX_new");
      break; // failed
    }
    
    // To remove SSLv2 & 3 use SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3
    // To remove compression use SSL_OP_NO_COMPRESSION
    const long flags = SSL_OP_ALL ;
    long old_opts = SSL_CTX_set_options(ctx, flags);
    (void)old_opts; // Unused variable
    
    sbio = BIO_new_ssl_connect(ctx);
    
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_new_ssl_connect\n");
    assert(sbio != NULL);
    if(!(sbio != NULL)) {
      print_ssl_error_string(ssl_err, "BIO_new_ssl_connect");
      break; // failed
    }
    
    res = BIO_set_conn_hostname(sbio, host);
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_set_conn_hostname\n");
    assert(1 == res);
    if(!(1 == res)) {
      print_ssl_error_string(ssl_err, "BIO_set_conn_hostname");
      break; // failed
    }
    
   // This copies an internal pointer. No need to free.
    BIO_get_ssl(sbio, &ssl);
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_get_ssl\n");
    assert(ssl != NULL);
    if(!(ssl != NULL)) {
      print_ssl_error_string(ssl_err, "BIO_get_ssl");
      break;
    }
    
    res = BIO_do_connect(sbio);
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_do_connect\n");
    if (verbose) fprintf(stderr,"--- ssl state: %s\n",SSL_state_string_long(ssl));
    assert(1 == res);
    if(!(1 == res)) {
      print_ssl_error_string(ssl_err, "BIO_do_connect");
      break; // failed
    }
    
    res = BIO_do_handshake(sbio);
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_do_handshake\n");
    if (verbose) fprintf(stderr,"--- ssl state: %s\n",SSL_state_string_long(ssl));
    assert(1 == res);
    if(!(1 == res)) {
      print_ssl_error_string(ssl_err, "BIO_do_handshake");
      break; // failed
    }
    
    // This is where to perform X509 verification

    // Read and write can now be performed
    
    // Send heartbeat with 8 bytes extra payload requested
    my_tls1_heartbeat(ssl,16,8,16);
    
    socket = (int)BIO_get_fd(sbio, &socket);
    
    // Send close notify to force response from server
    // Unaffected servers simply don't respond to invalid heartbeat requests
    // so response from affected servers will be a record of heartbeat type
    // (followed by an alert, but we only read the first record)
    // and unaffected will respond with an alert.
    unsigned char minor_version = ssl->session->ssl_version & 0x03;
    // socket may already be closed by peer, ignore SIGPIPE
    struct sigaction new_actn, old_actn;
    new_actn.sa_handler = SIG_IGN;
    sigemptyset (&new_actn.sa_mask);
    new_actn.sa_flags = 0;
    sigaction (SIGPIPE, &new_actn, &old_actn);
    
    // send close notify
    memcpy(&close_notify[server_minor_position], &minor_version, 1);
    write(socket, close_notify, close_notify_size);
    
    // restore signal handling
    sigaction (SIGPIPE, &old_actn, NULL);
    record = read_SSL_record(socket);
    
    if (record.data_size > 0 &&
        record.type == SSL_RECORD_TYPE__HEARTBEAT) {
      if (verbose) printf("probe_heartbleed -- Server responeded with heartbeat, i.e. vulnerable to Heartbleed\n");
      ret = EXIT_HEARTBLEED;
    }
    else if (record.data_size > 0 &&
             record.type == SSL_RECORD_TYPE__ALERT) {
      // Server not vulnerable
      if (verbose) printf("probe_heartbleed -- Server responded with alert, i.e. not vulnerable to Heartbleed\n");
     ret = EXIT_NO_HEARTBLEED;
    }
    else {
      printf("probe_heartbleed -- Server responded with the following unexpected record:");
      print_SSL_record_info(record);
      ret = EXIT_UNKNOWN_FAILURE;
    }
    
  } while (0);
  
  // socket may already be closed by peer, ignore SIGPIPE
  struct sigaction new_actn, old_actn;
  new_actn.sa_handler = SIG_IGN;
  sigemptyset (&new_actn.sa_mask);
  new_actn.sa_flags = 0;
  sigaction (SIGPIPE, &new_actn, &old_actn);
  
  SSL_shutdown(ssl);
  
  // restore signal handling
  sigaction (SIGPIPE, &old_actn, NULL);
  close(socket);
  
  // Free up memory and close down connection
  if(sbio != NULL)
    BIO_free_all(sbio);
  if(NULL != ctx)
    SSL_CTX_free(ctx);

  return ret;
}
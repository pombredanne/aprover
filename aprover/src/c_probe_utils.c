//
//  c_probe_utils.c
//  aprover
//
//  Created by Martin Andersson on 24/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Implements useful functions for probes implemented in C and defines relevant constants.
 */

#include "c_probe_utils.h"

// Initialize raw data for clienthello
const size_t clienthello_data_size = 343;
const size_t clienthello_header_size = 5;
const size_t clienthello_data2_size = 305;
const size_t before_data2_size = 38;
const size_t close_notify_size = 7;
const int server_minor_position = 2;

unsigned char clienthello_data[343] = {
  SSL_HANDSHAKE_TYPE__CLIENT_HELLO, // type
  0x00, 0x01, 0x53, // size
  SSL_MAJOR_VERSION__SSL3_TLS, // client major version
  SSL_MINOR_VERSION__TLS1_2}; // client minor version

unsigned char clienthello_data2[305] = { 0x00, // Length of session id
  0x00, 0xb6, // Length of supoprted ciphers 0xb6 = 182
  0xc0, 0x30, 0xc0, 0x2c, 0xc0, 0x28, 0xc0, 0x24, 0xc0, 0x14, 0xc0, 0x0a, 0x00, 0xa5,
  0x00, 0xa3, 0x00, 0xa1, 0x00, 0x9f, 0x00, 0x6b, 0x00, 0x6a, 0x00, 0x69, 0x00, 0x68, 0x00, 0x39,
  0x00, 0x38, 0x00, 0x37, 0x00, 0x36, 0x00, 0x88, 0x00, 0x87, 0x00, 0x86, 0x00, 0x85, 0xc0, 0x32,
  0xc0, 0x2e, 0xc0, 0x2a, 0xc0, 0x26, 0xc0, 0x0f, 0xc0, 0x05, 0x00, 0x9d, 0x00, 0x3d, 0x00, 0x35,
  0x00, 0x84, 0xc0, 0x2f, 0xc0, 0x2b, 0xc0, 0x27, 0xc0, 0x23, 0xc0, 0x13, 0xc0, 0x09, 0x00, 0xa4,
  0x00, 0xa2, 0x00, 0xa0, 0x00, 0x9e, 0x00, 0x67, 0x00, 0x40, 0x00, 0x3f, 0x00, 0x3e, 0x00, 0x33,
  0x00, 0x32, 0x00, 0x31, 0x00, 0x30, 0x00, 0x9a, 0x00, 0x99, 0x00, 0x98, 0x00, 0x97, 0x00, 0x45,
  0x00, 0x44, 0x00, 0x43, 0x00, 0x42, 0xc0, 0x31, 0xc0, 0x2d, 0xc0, 0x29, 0xc0, 0x25, 0xc0, 0x0e,
  0xc0, 0x04, 0x00, 0x9c, 0x00, 0x3c, 0x00, 0x2f, 0x00, 0x96, 0x00, 0x41, 0x00, 0x07, 0xc0, 0x11,
  0xc0, 0x07, 0xc0, 0x0c, 0xc0, 0x02, 0x00, 0x05, 0x00, 0x04, 0xc0, 0x12, 0xc0, 0x08, 0x00, 0x16,
  0x00, 0x13, 0x00, 0x10, 0x00, 0x0d, 0xc0, 0x0d, 0xc0, 0x03, 0x00, 0x0a, 0x00, 0x15, 0x00, 0x12,
  0x00, 0x0f, 0x00, 0x0c, 0x00, 0x09, 0x00, 0xff, // Supported ciphers
  0x02, // Length of compression methods
  0x01, 0x00, // Compression methods
  0x00, 0x73, // Extension data length = 115
  0x00, 0x0b, // TLS extension: EC points format
  0x00, 0x04, // Length = 4
  0x03, 0x00, 0x01, 0x02, // EC point format data
  0x00, 0x0a, // TLS extension: elliptic curves
  0x00, 0x3a, // Length = 58
  0x00, 0x38, 0x00, 0x0e, 0x00, 0x0d, 0x00, 0x19, 0x00, 0x1c, 0x00, 0x0b, 0x00, 0x0c, 0x00, 0x1b,
  0x00, 0x18, 0x00, 0x09, 0x00, 0x0a, 0x00, 0x1a, 0x00, 0x16, 0x00, 0x17, 0x00, 0x08, 0x00, 0x06,
  0x00, 0x07, 0x00, 0x14, 0x00, 0x15, 0x00, 0x04, 0x00, 0x05, 0x00, 0x12, 0x00, 0x13, 0x00, 0x01,
  0x00, 0x02, 0x00, 0x03, 0x00, 0x0f, 0x00, 0x10, 0x00, 0x11, // elliptic curves data
  0x00, 0x23, // TLS extension: session ticket
  0x00, 0x00, // Length = 0
  0x00, 0x0d, // TLS extension: signature algorithms
  0x00, 0x20, // Length = 32
  0x00, 0x1e, 0x06, 0x01, 0x06, 0x02, 0x06, 0x03, 0x05, 0x01, 0x05, 0x02, 0x05, 0x03, 0x04, 0x01,
  0x04, 0x02, 0x04, 0x03, 0x03, 0x01, 0x03, 0x02, 0x03, 0x03, 0x02, 0x01, 0x02, 0x02, 0x02, 0x03,
  // signature algorithms data
  0x00, 0x0f, // TLS extension: heartbeat
  0x00, 0x01, // Length = 1
  0x01 // heartbeat mode
};

unsigned char clienthello_header[5] = {SSL_RECORD_TYPE__HANDSHAKE,
  SSL_MAJOR_VERSION__SSL3_TLS,
  SSL_MINOR_VERSION__TLS1_0, // server will respond with higher version if supported
  0x01, 0x57};

unsigned char close_notify[7] = {SSL_RECORD_TYPE__ALERT,
  SSL_MAJOR_VERSION__SSL3_TLS,
  0x00, // needs to be set according to negotiatied protocol
  0x00, 0x02, // size
  SSL_ALERT_WARNING, SSL_ALERT_CLOSE_NOTIFY};


// Prints buffer in hex format
void print_hex(const unsigned char* buffer, size_t bytes)  {
  int count = 0;
  for (size_t i = 0; i < bytes; i++) {
    if (i % 16 == 0) {
      printf("\n%d\t", count);
      count += 16;
    }
    else if (i % 8 == 0)
      printf("-");
    else printf(" ");
    printf("%02x", buffer[i]);
  }
  printf("\n");
}

// Prints information about SSL_record_info
void print_SSL_record_info(struct SSL_record_info record) {
  printf("\tSSL_record type: 0x%02x, minor version: 0x%02x, data_size: %lu\n",
         record.type, record.minor_version, record.data_size);
  if (record.type == SSL_RECORD_TYPE__ALERT) {
    printf("\t\tsubtype: 0x%02x, alert: 0x%02x\n", record.subtype, record.subsubtype);
  }
  else {
    printf("\t\tsubtype: 0x%02x\n", record.subtype);
  }
}

// Reads an SSL record from given socket and
// returns SSL record information
struct SSL_record_info read_SSL_record(const int socket) {
  struct SSL_record_info record;
  unsigned char* buf = (unsigned char*)malloc(sizeof(unsigned char)*BUF_SIZE);
  memset(buf, 0, BUF_SIZE);
  ssize_t bytes_read = 0;
  ssize_t newly_read = 0;
  time_t start = time(NULL);
  
  // Read until all of header read or timeout
  // check for TCP FIN, i.e. closed by peer
  ssize_t header_size = 5;
  unsigned char c;
  while (bytes_read < header_size &&
         ((time(NULL)-start) < READ_TIMEOUT) ) {
    ssize_t peek = recv(socket, &c, 1, 0); // MSG_PEEK is not implemented in NaCl
    if (peek == 0) {
    // ...handle FIN from A
      if (UTILS_VERBOSE) printf("c_probe_utils -- received TCP FIN (peek == 0)\n");
      printf("c_probe_utils -- TCP FIN sent by peer.\n");
      struct SSL_record_info record = {0, 0, 0, 0, 0}; // set all values to 0
      close(socket);
      return record;
    } else if (peek != 1) {
      // ...handle errors 
      printf("c_probe_utils -- Error occured when checking connection state.\n");
      printf("-- errno %d : %s\n", errno, strerror(errno));
      struct SSL_record_info record = {0, 0, 0, 0, 0}; // set all values to 0
      close(socket);
      return record;
    }
    buf[bytes_read] = c;
    bytes_read += 1;
    
    newly_read = read(socket, &buf[bytes_read], (unsigned long)(header_size-bytes_read));
    if (newly_read > 0)
      bytes_read += newly_read;
    if (bytes_read < header_size)
      sleep(1); // 1 sec
  }
  
  if (UTILS_VERBOSE) printf("c_probe_utils -- bytes_read = %zi\n", bytes_read);
  
  if (bytes_read < header_size) {
    printf("c_probe_utils -- read_SSL_record -- Could not read entire header. Connection may have timed out.\n");
    struct SSL_record_info record = {0, 0, 0, 0, 0}; // set all values to 0
    close(socket);
    return record;
  }
  if ( UTILS_VERBOSE ) {
    printf("c_probe_utils -- INFO: read SSL header: ");
    print_hex(buf, (size_t)bytes_read);
    printf("\n");
  }
  
  // Extract the information
  record.type = buf[0];
  record.minor_version = buf[2];
  record.data_size =(((long)(buf[3]))<< 8)| ((long)(buf[4]));
  if (record.data_size < 0) {
    printf("c_probe_utils -- ERROR read_SSL_record -- negative data size.\n");
    struct SSL_record_info record = {0, 0, 0, 0, 0}; // set all values to 0
    close(socket);
    return record;
  }
  
  // Clear buffer for new read
  memset(buf, 0, BUF_SIZE);
  
  // Read the data until done or timeout
  bytes_read = 0;
  newly_read = 0;
  while (bytes_read < record.data_size &&
         ((time(NULL)-start) < READ_TIMEOUT) ) {
    newly_read = read(socket, &buf[bytes_read], (unsigned long)(record.data_size-bytes_read));
    if (newly_read > 0)
      bytes_read += newly_read;
    if (bytes_read < record.data_size)
      sleep(1); // 1 sec
  }
  
  if (bytes_read != record.data_size) {
    printf("probe_timstamp_random -- WARNING: could not read correct amount of data. Expected %lu bytes, got %zu\n",
           record.data_size, bytes_read);
    close(socket);
    struct SSL_record_info record = {0, 0, 0, 0, 0}; // set all values to 0
    return record;
  }
  
  // Extract subtype info
  record.subtype = buf[0];
  
  // Alert records also have a subsubtype
  if (record.type == SSL_RECORD_TYPE__ALERT && bytes_read >= 2) {
    record.subsubtype = buf[1];
  }
  else {
    record.subsubtype = 0;
  }
  
  
  if (UTILS_VERBOSE) { print_SSL_record_info(record); }
  free(buf);
  return record;
}

// Lookup IP address for given host
// This function is based on the example from the man-pages
// http://man7.org/linux/man-pages/man3/getaddrinfo.3.html
in_addr_t lookup_host(const char* host, int port) {
  if (UTILS_VERBOSE) printf("c_probe_utils -- INFO: Entering lookup_host for %s:%d\n", host, port);
  
  in_addr_t host_addr;
  struct addrinfo hints;
  struct addrinfo *result, *rp;
  int sfd, s;
  
  /* Obtain address(es) matching host/port */
  hints.ai_family = AF_INET;    /* Allow IPv4 (AF_INET) or IPv6 (AF_INET6) or both AF_UNSPEC */
  hints.ai_socktype = 0; /* Local network uses UDP and web uses TCP  */
  hints.ai_flags = 0;
  hints.ai_protocol = 0;          /* Any protocol */
  
  char* port_str = (char*)malloc(sizeof(char)*16);
  sprintf(port_str, "%d", port);
  s = getaddrinfo(host, port_str, &hints, &result);
  if (s != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
    // (uintptr_t)
    return (uintptr_t) NULL; // % blkLen;
  }
  free(port_str);
  
  /* getaddrinfo() returns a list of address structures.
   Try each address until we successfully connect.
   If socket (or connect) fails, we (close the socket
   and) try the next address. */
  
  for (rp = result; rp != NULL; rp = rp->ai_next) {
    sfd = socket(rp->ai_family, rp->ai_socktype,
                 rp->ai_protocol);
    if (sfd == -1)
      continue;
    
    if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1) {
      // Success
      
      //check the family version of client IP address, so you
      //can know where to cast, either to sockaddr_in or sockaddr_in6
      //and then grab the addr after casting
      if (rp->ai_addr->sa_family == AF_INET) {
        // IPv4
        struct sockaddr_in *ipv4 = (struct sockaddr_in *)rp->ai_addr;
        host_addr = ipv4->sin_addr.s_addr;
      }else{
        // IPv6
        printf("c_probe_utils -- ERROR errno %s: Could not look up host or host may have IPv6 address.\n", strerror(errno));
        return (uintptr_t)NULL;
        /* IPv6 should be something along these lines ...
         struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)rp->ai_addr;
         host_addr = ipv6->sin6_addr.s6_addr;
         */
      }
      break;
    }
    
    
    close(sfd);
  }
  
  if (rp == NULL) {               // No address succeeded
    printf("c_probe_utils -- ERROR errno %s: Could not look up host: %s.\n", strerror(errno), host);
    return (uintptr_t)NULL;
  }
  
  freeaddrinfo(result);           // No longer needed

  return host_addr;
}

// Set up a TCP socket
int setup_TCP_socket(int* sock, struct sockaddr_in* server_addr, const char* host, const int port) {
  *sock = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP);
  
  if (*sock == -1) {
    printf("c_probe_utils -- setup_TCP_socket -- errno: %d\n", errno);
    return -1;
  }
  
  struct timeval tv;
  tv.tv_sec = READ_TIMEOUT;
  tv.tv_usec = 0;  // Not init'ing this can cause strange errors
  
  setsockopt(*sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval));
  
  memset (server_addr, '\0', sizeof(*server_addr));
  server_addr->sin_family      = AF_INET;
  // Server Port number
  server_addr->sin_port        = htons(port);
  // Server IP
  server_addr->sin_addr.s_addr = lookup_host(host, port);
  
  // Establish a TCP/IP connection to the SSL client
  return connect(*sock, (struct sockaddr*) server_addr, sizeof(*server_addr));
}

// Sends a clienthello on the given socket
void send_clienthello(const int sock) {
  // Get random bytes
  unsigned char* rand_bytes = (unsigned char*)malloc(sizeof(unsigned char)*28);
  RAND_bytes(rand_bytes, 28);
  
  // Get seconds since epoch
  time_t rawtime = time(NULL); // now
  
  unsigned char* epoch_time = (unsigned char*)malloc(sizeof(unsigned char)*4);
  //unsigned long n = rawtime;
  epoch_time[0] = (rawtime >> 24) & 0xFF;
  epoch_time[1] = (rawtime >> 16) & 0xFF;
  epoch_time[2] = (rawtime >> 8) & 0xFF;
  epoch_time[3] = rawtime & 0xFF;
  
  // Debug info
  if (UTILS_VERBOSE) {
    printf("c_probe_utils --  INFO: rand bytes");
    print_hex(rand_bytes, 28);
    
    printf("\nc_probe_utils -- INFO: epoch time (%ju sec)", rawtime);
    print_hex(epoch_time, 4);
    printf("\n");
  }
    
  // Construct clienthello as
  // [ clienthello[6], epoch_time[4], rand_bytes[28], clienthello_data2[305] ]
  memcpy(&clienthello_data[6], epoch_time, 4);
  memcpy(&clienthello_data[10], rand_bytes, 28);
  memcpy(&clienthello_data[38], clienthello_data2, 305);
  
  // Write client_hello
  write(sock, clienthello_header, 5);
  write(sock, clienthello_data, 343);
  
  free(rand_bytes);
  free(epoch_time);
}

// Send close notify if no alert received
void send_close_notify(const int sock, struct SSL_record_info record) {
  if (record.data_size > 0) {
    // socket may already be closed by peer, ignore SIGPIPE
    struct sigaction new_actn, old_actn;
    new_actn.sa_handler = SIG_IGN;
    sigemptyset (&new_actn.sa_mask);
    new_actn.sa_flags = 0;
    sigaction (SIGPIPE, &new_actn, &old_actn);
    
    // send close notify
    memcpy(&close_notify[server_minor_position], &record.minor_version, 1);
    write(sock, close_notify, close_notify_size);
    
    // restore signal handling
    sigaction (SIGPIPE, &old_actn, NULL);
  }
  // close socket
  close(sock);
}

// Initialize the OpenSSL library
void init_openssl_library(void) {
  (void)SSL_library_init();
  
  // SSL_load_error_strings loads both libssl and libcrypto strings
  SSL_load_error_strings();
  
  // OpenSSL_config may or may not be called internally, based on
  //  some #defines and internal gyrations. Explicitly call it
  //  *IF* you need something from openssl.cfg, such as a
  //  dynamically configured ENGINE.
  // if used: #include <openssl/conf.h>
  //OPENSSL_config(NULL);
}

// Prints SSL error message together with given label
void print_ssl_error_string(unsigned long err, const char* const label) {
  const char* const str = ERR_reason_error_string(err);
  if(str)
    fprintf(stdout, "%s: %s\n", label, str);
  else
    fprintf(stdout, "%s failed: %lu (0x%lx)\n", label, err, err);
}

//*/


// Send a modified heartbeatrequest
// Based on the library function tls1_heartbeat from ssl/t1_lib.c
int my_tls1_heartbeat(SSL *s, unsigned int payload, unsigned int extra_payload, unsigned int padding)
{
  unsigned char *buf, *p;
  int ret = 0;
  //  unsigned int payload = 18; /* Sequence number + random bytes */
  //  unsigned int padding = 16; /* Use minimum padding */
  if  (payload < 2) {
    payload = 2;
  }
  if (padding < 16) {
    padding = 16;
  }
  //  unsigned int extra_payload = 0; // To exploit heartbeat
  unsigned int seq_nr = 100; // 0x64
  
  // Do not check if payload and padding is too long
  // beacuase we want to be able to probe for this
  
  /* Create HeartBeat message, we just use a sequence number
   * as payload to distuingish different messages and add
   * some random stuff.
   *  - Message Type, 1 byte
   *  - Payload Length, 2 bytes (unsigned int)
   *  - Payload, the sequence number (2 bytes uint)
   *  - Payload, random bytes
   *  - Padding
   */
  buf = OPENSSL_malloc(1 + 2 + payload + padding);
  p = buf;
  /* Message Type */
  *p++ = 1; // request
  /* Payload length (18 bytes here) */
  s2n(payload+extra_payload, p);
  /* Sequence number */
  s2n(seq_nr, p);
  /* 16 random bytes */
  RAND_pseudo_bytes(p, (int)payload-2);
  p += (payload-2);
  /* Random padding */
  RAND_pseudo_bytes(p, (int)padding);
  
  ret = ssl3_write_bytes(s, SSL_RECORD_TYPE__HEARTBEAT, buf, (int)(3 + payload + padding));
  if (ret >= 0)
  {
    if (s->msg_callback)
      s->msg_callback(1, s->version, SSL_RECORD_TYPE__HEARTBEAT,
                      buf, 3 + payload + padding,
                      s, s->msg_callback_arg);
  }
  
  OPENSSL_free(buf);
  
  return ret;
}







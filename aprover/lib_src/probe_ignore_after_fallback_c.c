//
//  probe_ignore_after_fallback.c
//
//
//  Created by Martin Andersson on 07/07/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Some versions ignores ciphers suites following the TLS_FALLBACK_SCSV in the ClientHello message.
 * By starting to list TLS_FALLBACK_SCSV in the cipher list this results in a handshake failure
 * on affected servers. Unaffected servers will respond with ServerHello.
 *
 * Returns 0 if server is not affected
 * Returns 1 if server is affected
 * Higher return codes are resevered for errors. See c_probe_utils.h.
 */

#include "c_probe_utils.h"

// Probes the server for the feature
// See description at top of file for return codes used.
int c_probe_ignore_after_fallback(const char* host, int port, int verbose)
{
  int ret = -1; // return value
  int     err;

  // For the socket
  int     sock;
  struct sockaddr_in server_addr;
  
  // Set up a TCP socket
  err = setup_TCP_socket(&sock, &server_addr, host, port);
  if (err == -1) {
    printf("errno: %d\n", errno);
    printf("probe_ignore_after_fallback -- ERROR: Connect of TCP socket failed.\n");
    return EXIT_CONNECT_FAILURE;
  }
  
  // Get random bytes
  unsigned char* rand_bytes = (unsigned char*)malloc(sizeof(unsigned char)*28);
  RAND_bytes(rand_bytes, 28);
  
  // Get seconds since epoch
  time_t rawtime = time(NULL); // now
  
  unsigned char* epoch_time = (unsigned char*)malloc(sizeof(unsigned char)*4);
  //unsigned long n = rawtime;
  epoch_time[0] = (rawtime >> 24) & 0xFF;
  epoch_time[1] = (rawtime >> 16) & 0xFF;
  epoch_time[2] = (rawtime >> 8) & 0xFF;
  epoch_time[3] = rawtime & 0xFF;
  
  // Debug info
  if (verbose) {
    printf("## INFO: rand bytes");
    print_hex(rand_bytes, 28);
    
    printf("\n## INFO: epoch time (%ju sec)", rawtime);
    print_hex(epoch_time, 4);
    printf("\n");
  }
  
  /**********************************************/
  /* Client hello constructed and ready for use */
  /**********************************************/
  
  // Store original values
  unsigned char* old_values = (unsigned char*)malloc(sizeof(unsigned char)*2);
  memcpy(old_values, &clienthello_data2[3], 2);
  
  // Set first cipher suite to be TLS_FALLBACK_SCSV i.e 0x56 0x00
  clienthello_data2[3] = 0x56;
  clienthello_data2[4] = 0x00;
  
  // Construct clienthello_data[343] as
  // [ clienthello_data[6], epoch_time[4], rand_bytes[28], clienthello_data2[305] ]
  memcpy(&clienthello_data[6], epoch_time, 4);
  memcpy(&clienthello_data[10], rand_bytes, 28);
  memcpy(&clienthello_data[38], clienthello_data2, clienthello_data2_size);

  // Write client_hello
  write(sock, clienthello_header, clienthello_header_size);
  write(sock, clienthello_data, clienthello_data_size);
  
  if (verbose) printf("probe_ignore_after_fallback -- \
INFO: Waiting for reply.\n");
  
  // Affected servers will respond with handshake_failure
  // and unaffected will respond with server_hello
  struct SSL_record_info record;
  record = read_SSL_record(sock);
  if (record.data_size > 0 &&
      record.type == SSL_RECORD_TYPE__ALERT &&
      record.subsubtype == SSL_ALERT_HANDSHAKE_FAILURE ) {
    // Server responded with alert
    if (verbose) printf("probe_ignore_after_fallback -- \
INFO: Affected server responded with expected alert: 0x%02x\n",
                                       record.subsubtype);
    ret = 1;
  } else if (record.data_size > 0 &&
             record.type == SSL_RECORD_TYPE__HANDSHAKE &&
             record.subtype == SSL_HANDSHAKE_TYPE__SERVER_HELLO) {
    if (verbose) printf("probe_ignore_after_fallback -- \
INFO: Unaffected server responded with expected server_hello\n");
    ret = 0;
  } else {
    // Server did not respond with expected alert or server_hello
    if (verbose) printf("probe_ignore_after_fallback -- \
INFO: Server did not respond with handshake_failure alert or server_hello\n");
    ret = EXIT_UNKNOWN_FAILURE;
  }
  
  if (verbose || ret == EXIT_UNKNOWN_FAILURE) {
    printf("probe_ignore_after_fallback -- received following record:\n");
    print_SSL_record_info(record);
  }
  
  send_close_notify(sock, record);
  
  // Reset payload length
  memcpy(&clienthello_data2[3], old_values, 2);
  
  free(old_values);
  free(rand_bytes);
  free(epoch_time);
  
  return ret;
}

//
//  probe_heartbeat.c
//  aprover
//
//  Created by Martin Andersson on 08/07/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Checks for support of the heartbeat extension
 * 
 * Returns 1 if the Heartbeat extension is supported and peer is allowed to send
 * and 0 otherwise
 */

#include "c_probe_utils.h"

// Return codes
#define EXIT_NO_HEARTBEAT             0
#define EXIT_HEARTBEAT                1

// Probes the server for the feature
// See description at top of file for return codes used.
int c_heartbeat_probe(const char* host, int port, int verbose)
{
  int ret = EXIT_NO_HEARTBEAT; // return value
  int     err;
  
  int     sock;
  struct sockaddr_in server_addr;
  /* ----------------------------------------------- */
  // Set up a TCP socket
  
  err = setup_TCP_socket(&sock, &server_addr, host, port);
  
  if (err == -1) {
    printf("errno: %d\n", errno);
    printf("probe_heartbeat -- ERROR: Connect of TCP socket failed.\n");
    close(sock);
    return EXIT_CONNECT_FAILURE;
  }
  
  /* ----------------------------------------------- */
  // Perform SSL Handshake
  
  send_clienthello(sock);
  
  // Read server_hello
  struct SSL_record_info record;
  
  unsigned char buf[BUF_SIZE];
  memset(buf, 0, BUF_SIZE);
  ssize_t bytes_read = 0;
  
  // Read record header
  bytes_read = read(sock, buf, 5);
  if (bytes_read < 5 || bytes_read > BUF_SIZE) {
    printf("probe_heartbeat -- no data or data can not fit inside buffer.\n");
    close(sock);
    return EXIT_SSL_RECORD_FAILURE;
  }
  if ( verbose ) {
    printf("probe_heartbeat -- INFO: read SSL header: ");
    print_hex(buf, (size_t)bytes_read);
    printf("\n");
  }
  
  // Extract the information
  record.type = buf[0];
  record.minor_version = buf[2];
  char size_array[7];
  sprintf(size_array, "0x%02x%02x", buf[3], buf[4]);
  record.data_size = strtol(size_array, NULL, 0);
  if (record.data_size < 0) {
    printf("probe_heartbeat -- ERROR -- negative data size.\n");
    close(sock);
    return EXIT_SSL_RECORD_FAILURE;
  }
  
  
  // Clear buffer for new read
  memset(buf, 0, BUF_SIZE);
  
  // Read the data
  bytes_read = 0;
  while (bytes_read < record.data_size) {
    bytes_read += read(sock, &buf[bytes_read], (unsigned long)(record.data_size-bytes_read));
  }
  if (bytes_read != record.data_size) {
    printf("probe_heartbeat -- WARNING: could not read correct amount of data. Expected %lu bytes, got %zu\n",
           record.data_size, bytes_read);
    close(sock);
    return EXIT_SSL_RECORD_FAILURE;
  }
  
  if (verbose) {
    print_SSL_record_info(record);
    printf("probe_heartbeat -- Data received: ");
    print_hex(buf, (size_t)record.data_size);
  }
  
  // Extract subtype info
  record.subtype = buf[0];
  
  // Check so that server_hello was received
  if (record.data_size <= 0 ||
      record.type != SSL_RECORD_TYPE__HANDSHAKE ||
      record.subtype != SSL_HANDSHAKE_TYPE__SERVER_HELLO) {
    printf("probe_heartbeat -- ERROR errno %s: Expected server_hello not received. Aborting.\n", strerror(errno));
    printf("probe_heartbeat -- Recieved the following record instead: \n");
    print_SSL_record_info(record);
    close(sock);
    return EXIT_HANDSHAKE_FAILURE;
  }
  
  // Check so that session ID is 0 as expected
  if (buf[38] != 0x00) {
    printf("probe_heartbeat -- ERROR: Expected session ID to be 0 but it is %d\n", buf[38]);
    close(sock);
    return EXIT_HANDSHAKE_FAILURE;
  }
  
  // Check so that ServerHello length is long enough to accomodate extensions
  if (record.data_size < 43) {
    printf("probe_heartbeat -- ERROR: Expected data size of ServerHello to be at least 44 bytes,\
 only got %li bytes\n", record.data_size);
    close(sock);
    return EXIT_HANDSHAKE_FAILURE;
  }
  
  // Parse the extensions to find heartbeat 0x00 0x0f
  int tot_ext_length = (((int)(buf[42]))<< 8)| ((int)(buf[43]));
  int ext_length_parsed = 0;
  long ext_data = 0;
  while (!(buf[44+ext_length_parsed] == 0x00 &&
          buf[44+ext_length_parsed+1] == 0x0f) &&
         ext_length_parsed < tot_ext_length) {
    // ''read' ext type
    ext_length_parsed += 2;
    // 'read' ext data length
    ext_data = (((long)(buf[44+ext_length_parsed]))<< 8)| ((long)(buf[44+ext_length_parsed+1]));
    if (verbose) {
      printf("probe_heartbeat -- Extension: 0x%02x 0x%02x of length %li\n",
             buf[44+ext_length_parsed-2],buf[44+ext_length_parsed-2+1], ext_data);
    }
    ext_length_parsed += (2+ext_data); // the extension type, the length specifier and the data itself
    if (verbose) {
      printf("probe_heartbeat -- Extension data parsed: %d out of total %d\n", ext_length_parsed, tot_ext_length);
    }
    
  }
  
  // Heartbeat found or end of extensions
  if (buf[44+ext_length_parsed] == 0x00 &&
      buf[44+ext_length_parsed+1] == 0x0f) {
    // Heartbeat found
    if (verbose) {
      printf("probe_heartbeat -- Found heartbeat extension.\n");
    }
    // 'read' extension type
    ext_length_parsed += 2;
    // 'read' ext data length
    ext_data = (((int)(buf[44+ext_length_parsed]))<< 8)| ((int)(buf[44+ext_length_parsed+1]));
    ext_length_parsed += 2;
    if (ext_data == 1 &&
        buf[44+ext_length_parsed] == 0x01) {
      // Heartbeat extensions always only have 1 byte of data
      // data = 0x01 peer_allowed_to_send
      // data = 0x02 peer_not_allowed_to_send
      printf("probe_heartbeat -- Heartbeat: peer_allowed_to_send.\n");
      ret = EXIT_HEARTBEAT;
    }
  }
  
  
  /* ----------------------------------------------- */
  // Close down connection
  
  send_close_notify(sock, record);
  
  return ret;
}

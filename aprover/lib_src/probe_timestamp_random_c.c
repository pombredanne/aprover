//
//  probe_timestamp_random.c
//  aprover
//
//  Created by Martin Andersson on 25/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Affected versions include a timestamp in the random bytes sent in the
 * server_hello message during the handshake
 * Returns 0 if the server DOES NOT include a timestamp in the random bytes.
 * Returns 1 if the server DOES include a timestamp in the random bytes.
 * Higher return codes are resevered for errors. 
 *
 * Note that versions after 0.9.8zg and 1.0.0s may or may not be affected 
 * by this depending on if the change is implemented in those branches as well.
 *
 * Affects version: 0.9.8:0.9.8zg, 1.0.0:1.0.0s, 1.0.1:1.0.1e
 *
 * Loaded by probe_timestamp_random.cpp
 */

#include "c_probe_utils.h"

// Return codes
#define EXIT_NO_TIMESTAMP             0
#define EXIT_TIMESTAMP                1


// Probes the server for the feature
// See description at top of file for return codes used.
int c_timestamp_random_probe(const char* host, int port, int verbose)
{
  int timestamp = EXIT_UNKNOWN_FAILURE; // return value
  int     err;
  
  int     sock;
  struct sockaddr_in server_addr;
  /* ----------------------------------------------- */
  // Set up a TCP socket
  
  err = setup_TCP_socket(&sock, &server_addr, host, port);
  
  if (err == -1) {
    printf("errno: %d\n", errno);
    printf("probe_timestamp_random -- ERROR: Connect of TCP socket failed.\n");
    close(sock);
    return EXIT_CONNECT_FAILURE;
  }
  
  /* ----------------------------------------------- */
  // Perform SSL Handshake
  
  send_clienthello(sock);
  
  // Read server_hello
  struct SSL_record_info record;
  
  unsigned char* buf = (unsigned char*)malloc(sizeof(unsigned char)*BUF_SIZE);
  memset(buf, 0, BUF_SIZE);
  ssize_t bytes_read = 0;
  
  // Read record header
  bytes_read = read(sock, buf, 5);
  if (bytes_read < 5 || bytes_read > BUF_SIZE) {
    printf("probe_timestamp_random -- read_SSL_record -- no data or data can not fit inside buffer.\n");
    close(sock);
    return EXIT_SSL_RECORD_FAILURE;
  }
  if ( verbose ) {
    printf("probe_timestamp_random -- INFO: read SSL header: ");
    print_hex(buf, (size_t)bytes_read);
    printf("\n");
  }
  
  // Extract the information
  record.type = buf[0];
  record.minor_version = buf[2];
  char size_array[7];
  sprintf(size_array, "0x%02x%02x", buf[3], buf[4]);
  record.data_size = strtol(size_array, NULL, 0);
  if (record.data_size < 0) {
    printf("probe_timestamp_random -- ERROR read_SSL_record -- negative data size.\n");
    close(sock);
    return EXIT_SSL_RECORD_FAILURE;
  }
  
  
  // Clear buffer for new read
  memset(buf, 0, BUF_SIZE);
  
  // Read the data
  bytes_read = 0;
  while (bytes_read < record.data_size) {
    bytes_read += read(sock, &buf[bytes_read], (unsigned long)(record.data_size-bytes_read));
  }
  if (bytes_read != record.data_size) {
    printf("probe_timestamp_random -- WARNING: could not read correct amount of data. Expected %lu bytes, got %zu\n",
           record.data_size, bytes_read);
    close(sock);
    return EXIT_SSL_RECORD_FAILURE;
  }
  
  // Extract subtype info
  record.subtype = buf[0];
  
  if (verbose) print_SSL_record_info(record);
  
  // Check so that server_hello was received
  if (record.data_size <= 0 ||
      record.type != SSL_RECORD_TYPE__HANDSHAKE ||
      record.subtype != SSL_HANDSHAKE_TYPE__SERVER_HELLO) {
    printf("probe_timestamp_random -- ERROR errno %s: Expected server_hello not received. Aborting.\n", strerror(errno));
    close(sock);
    return EXIT_HANDSHAKE_FAILURE;
  }
  
  unsigned char* server_random = (unsigned char*)malloc(sizeof(unsigned char)*4);
  server_random[0] = buf[6];
  server_random[1] = buf[7];
  server_random[2] = buf[8];
  server_random[3] = buf[9];
  
  time_t server_time;
  server_time = (server_random[0] << 24);
  server_time |= (server_random[1] << 16);
  server_time |= (server_random[2] << 8);
  server_time |= server_random[3];
  
  // Get seconds since epoch
  time_t rawtime = time(NULL); // now
  
  if (verbose)
   printf("\nServer time: %ju\nLocal time:  %ju\n", server_time, rawtime);
  
  // Compare rawtime and server_time
  // server_time is UTC, generated from time(NULL), same as rawtime
  // allow for a 2^9 = 512 seconds = 8.5 min
  time_t diff = 1 << 9;
  if ( (rawtime + diff) > server_time &&
      (rawtime - diff) < server_time )
    timestamp = EXIT_TIMESTAMP;
  else
    timestamp = EXIT_NO_TIMESTAMP;
  
  printf("probe_timestamp_random -- Supposed server time differed with %d sec.\n", abs((int)(rawtime-server_time)));
  printf("      Allowed diff, to still be considered a timestamp, is %ju sec.\n", diff);
  
  
  // Read any waiting records sent by the server.
  do {
    record = read_SSL_record(sock);
  } while (record.data_size > 0 &&
           record.type != SSL_RECORD_TYPE__ALERT &&
           (record.type == SSL_RECORD_TYPE__HANDSHAKE &&
            record.subtype != SSL_HANDSHAKE_TYPE__SERVER_DONE) );

  /* ----------------------------------------------- */
  // Close down connection
  
  send_close_notify(sock, record);
  
  free(buf);
  free(server_random);
  
  return timestamp;
}

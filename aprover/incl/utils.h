//
//  utils.h
//  aprover
//
//  Created by Martin Andersson on 24/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Utility functions used for output.
 */

#ifndef aprover_utils_h
#define aprover_utils_h

#include <stdlib.h>
#include <stdio.h>
#include <sstream>
#include <string>
#include <iostream>

#include "classes.h"

const int default_output_width = 72;

// Replace bad filename characters and whitespace with given char, defaults to '_'
// \ / < > | " : ? *
std::string replace_invalid_file_characters(std::string, const char = '_');

// Formats a string to fit within width and with a possible
// prefix leading to an indent
std::string format_string_for_output(const std::string s, const std::string prefix = "", int width = default_output_width, unsigned short indent = 0);


#endif

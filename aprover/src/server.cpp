//
//  server.cpp
//  aprover
//
//  Created by Martin Andersson on 09/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * The Server class represents the targeted server and has functions to
 * obtain and modify information about the server. The probes are passed the
 * targeted Server object which allows them to retrieve info and store results
 * about the server.
 */

#include "server.h"

using namespace std;

// Comparator function to compare the score of two version_result_t
bool score_cmp(version_result_t i, version_result_t j) {
  return  i.score < j.score;
}

// Default initialisation
Server::Server() {
  init_version_results();
}

// Initialisation with specified host and port
Server::Server(const string& host, const int port) :
host_(host), port_(port) {
  init_version_results();
}

// Need to delete the Feature pointers in probe_results_
Server::~Server() throw() { // c++11 noexcept {
  for(vector<probe_result_t>::iterator it = probe_results_.begin();
      it != probe_results_.end();
      ++it) {
    delete it->feature;
  }
}

// Extract the features probed from in probe_results_ and return them in a vector
vector<Feature> Server::get_features_probed() const {
  vector<Feature> res;
  for (vector<probe_result_t>::const_iterator it = probe_results_.begin();
       it != probe_results_.end();
       ++it) {
    res.push_back(*it->feature);
  }
  return res;
}

// Returns the version(s) with the highest score
vector<OPENSSL_VERSION> Server::get_most_likely_version() const {
  vector<OPENSSL_VERSION> res;
  int max_score = (*max_element(version_results_.begin(),
                                version_results_.end(),
                                score_cmp)).score;
  for (vector<version_result_t>::const_iterator it = version_results_.begin();
       it != version_results_.end();
       ++it) {
    if ( it->score == max_score )
      res.push_back(it->version);
  }
  return res;
}


// Returns the next feature to probe for
Feature Server::get_next_feature() const {
  Feature f;
  vector<Feature> res;
  vector<Feature> probed = get_features_probed();
  // Search for features not already probed for
  // Results are ordered by the Feature's destructiveness
  res = Database::get_features_excluding_probed(probed);
  if ( !res.empty() )
    f=res[0];
  else {
    // No features left to probe for
    // Catch this and check error msg
    throw server_error("DONE");
  }
  
  // The commented code below can be used if you want to choose the
  // next probe by first targeting the curently most likely versions
  // --------------------
  /*vector<OPENSSL_VERSION> most_likely = get_most_likely_version();
  vector<Feature> probed = get_features_probed();
  // Search for features targeting the most likely versions
  res = Database::search_affected_excluding_probed(most_likely, probed);
  if (!res.empty() )
    f=res[0];
  else {
    // Search for features not already probed for
    // Results are ordered by the Feature's destructiveness
    res = Database::get_features_excluding_probed(probed);
    if ( !res.empty() )
      f=res[0];
    else {
      // No features left to probe for
      // Catch this and check error msg
      throw server_error("DONE");
    }
  }*/
  return f;
}

// Prints information about the server to given stream
void Server::print(ostream& stream) const {
  stream << "Server: " << get_host() << " at port: " << get_port() << endl;
  print_probe_results(stream);
  stream << "Most likely versions: " << serialise_versions(get_most_likely_version()) << endl;
}

// Prints verbose information about the server to given stream
void Server::print_verbose(ostream& stream) const {
  stream << "Server: " << get_host() << " at port: " << get_port() << endl;

  time_t rawtime;
  struct tm * timeinfo;
  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  stream << "The current date/time (GMT) is: " << asctime (timeinfo) << endl;

  stream << "////////////////////////////" << endl;
  stream << "// Details about Features //" << endl;
  stream << "////////////////////////////" << endl;
  print_probe_results(stream, true);
  
  stream << "////////////////////////////" << endl;
  stream << "/////// Scores /////////////" << endl;
  stream << "////////////////////////////" << endl;
  stream << "Version : Score";
  unsigned short  per_line = 4;
  for(size_t i=0; i<version_results_.size(); ++i) {
    if (((unsigned short) i) % per_line == 0)
      stream << endl;
    stream << setw(7) << std::left << version_enum2str(version_results_[i].version) << setw(0) << std::right;
    stream << " : " << version_results_[i].score << "\t";
  }
  stream << endl << endl;
  
  stream << "////////////////////////////" << endl;
  stream << "/////// Info ///////////////" << endl;
  stream << "////////////////////////////" << endl;
  stream << "Info: " << endl << format_string_for_output(get_info()) << endl;
  
  stream << "////////////////////////////" << endl;
  stream << "/////// Summary ////////////" << endl;
  stream << "////////////////////////////" << endl;
  print_probe_results(stream, false);
  
  stream << "Most likely version(s): " <<
    serialise_versions(get_most_likely_version()) << endl;
}

// Prints verbose information about the server to given stream
void Server::print_web(ostream& stream) const {
  time_t rawtime;
  struct tm * timeinfo;
  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  stream << "The current date/time (GMT) is: " << asctime (timeinfo) << endl;
  

  stream << "Server: " << get_host() << " at port: " << get_port() << endl;
  
  stream << "////////////////////////////" << endl;
  stream << "/////// Info ///////////////" << endl;
  stream << "////////////////////////////" << endl;
  stream << "Info: " << endl << format_string_for_output(get_info()) << endl;
  
  stream << "////////////////////////////" << endl;
  stream << "/////// Summary ////////////" << endl;
  stream << "////////////////////////////" << endl;
  print_probe_results(stream, false);
  
  stream << "Most likely version(s): " <<
  serialise_versions(get_most_likely_version()) << endl << endl;

  
  stream << "////////////////////////////" << endl;
  stream << "// Details about Features //" << endl;
  stream << "////////////////////////////" << endl;
  print_probe_results(stream, true);
  
  stream << "////////////////////////////" << endl;
  stream << "/////// Scores /////////////" << endl;
  stream << "////////////////////////////" << endl;
  stream << "Version : Score";
  unsigned short  per_line = 4;
  for(size_t i=0; i<version_results_.size(); ++i) {
    if (((unsigned short) i) % per_line == 0)
      stream << endl;
    stream << setw(7) << std::left << version_enum2str(version_results_[i].version) << setw(0) << std::right;
    stream << " : " << version_results_[i].score << "\t";
  }
  stream << endl << endl;
}


// Prints the stored probe results in a neat fashion
void Server::print_probe_results(ostream& stream, bool verbose) const  {
  if (probe_results_.size() > 0) {
    int width = default_output_width;
    stream << "Feature" << setw(width-(int)strlen("Feature")) << "Status" << endl;
    stream << setfill('.');
    for (vector<probe_result_t>::const_iterator it = probe_results_.begin();
         it != probe_results_.end();
         ++it) {
      stream << it->feature->get_name();
      stream << setw(width-(int)it->feature->get_name().size());
      stream << std::right << status_as_string(it->status) << std::left << endl;
      if (verbose) {
        it->feature->print(stream);
        stream << endl;
      }
    }
    // Reset to normal values
    stream << std::internal;
    stream << setfill(' ');
    stream << setw(0);
  }
}

// Log info about the server to a file
// filename is defaulted to "" -> generate based on host and port
void Server::log(string filename) {
  if ( filename == "" ) {
   // filename = host_ + "_" + to_string(port_) + ".log"; // c++11
    stringstream ss;
    ss << host_ << "_" << port_ << ".log";
    filename = ss.str();
  }
  else
    filename = replace_invalid_file_characters(filename);
  // Put log files in separate directory
  filename = LOG_DIR + filename;
  
  int mkdir_status = mkdir(LOG_DIR.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  if (mkdir_status != 0 && errno != EEXIST) {
    cerr << "errno: " << errno << endl;
    cerr << "LOG_DIR: " << LOG_DIR << endl;
    throw server_error("Server::log -- Could not create directory for log files.\n");
  }
  
  
  ofstream log;
  log.open(filename);
  print_verbose(log);
  log.close();
  
}


// Add given feature to the vector of features probed for.
// And updates version_results accordingly
void Server::add_probe_results(Feature* f_ptr, int aff) {
  Feature* f_new = new Feature(*f_ptr);
  probe_results_.push_back(probe_result_t{f_new, aff});

  int conf = f_ptr->get_confidence();
  vector<OPENSSL_VERSION> targeted = f_ptr->get_affected_versions();
  
  if (aff == AFFECTED) {
    // Server was affected by probe
    for(size_t i=0; i<version_results_.size(); ++i) {
      if (contains(targeted, version_results_[i].version))
        // Server affected and version targeted
        version_results_[i].score += conf;
      else
       // Server affected and version not targeted
        version_results_[i].score += EXCLUDE_SCORE; // EXCLUDE_SCORE is defined as a negative value
    }
  }
  else if (aff == NOT_AFFECTED) {
    // Server was not affected by probe
    for(size_t i=0; i<version_results_.size(); ++i) {
      if (contains(targeted, version_results_[i].version))
        // Server not affected and version targeted
        version_results_[i].score -= conf;
      else
        // Server not affected and version not targeted
        version_results_[i].score += conf;
    }
  }
  // Do not do anything if probe failed or is disabled
}


// Perform fingerprinting of this server object
void Server::fingerprint(bool verbose, bool nacl) {
  
  // Check so that host and port is set
  if ( host_.empty() )
    throw server_error("Host must be set to perform fingerprinting of server\n");
  if ( port_ <= 0  )
    throw server_error("Port must be set to a positive integer to perform fingerprinting of server\n");
  
  if (!port_open()) {
    stringstream ss;
    ss << "Error: Port " << port_ <<
    " is not open on host " << host_ << endl;
    throw server_error(ss.str());
  }
  
  cout << "*** Fingerprinting server: " << get_host() << ":" << get_port() << endl;
  
  // Check protocol support first
  Feature f = Database::search_name("TLS 1.1 or 1.2 support", false);
  f.probe(*this, verbose);
  
  // Check heartbeat extension support
  f = Database::search_name("Heartbeat extension support", false);
  f.probe(*this, verbose);
  
  // Used for feature dependency
  Feature f_dep;
  
  vector<OPENSSL_VERSION> most_likely;
  try {
    while (true) {
      f = get_next_feature();
      // Check dependency
      // Loop until root of dependency is found
      bool dep_failed_disabled = false;
      while (!f.get_dependency().empty()) {
        if (verbose) cout << "Feature dependency -- " << f.get_name() << " depends on: " << f.get_dependency() << endl;
        try {
          f_dep = Database::search_name(f.get_dependency(), false);
        } catch (database_error& e) {
          stringstream ess;
          ess << "Feature dependency -- " << f.get_name() << " -- Disabled: Could not find prerequisite feature: " << f.get_dependency() << endl;
          cout << ess.str();
          this->add_info(ess.str());
          this->add_probe_results(&f, DISABLED);
          dep_failed_disabled = true;
          break;
        }
        
        // Check if already probed
        bool dep_already_probed = false;
        dep_failed_disabled = false;
        for (vector<probe_result_t>::iterator it = probe_results_.begin();
             it != probe_results_.end();
             ++it) {
          if (it->feature->get_name() == f_dep.get_name()) {
            dep_already_probed = true;
            if (it->status == FAILED || it->status == DISABLED)
              dep_failed_disabled = true;
            break;
          }
        }
        if (dep_failed_disabled) {
          stringstream ess;
          ess << "Feature dependency -- " << f.get_name() << " -- Error: Feature disabled because prerequisite feature: " << f.get_dependency();
          ess << " failed or is disabled." << endl;
          cout << ess.str();
          this->add_info(ess.str());
          this->add_probe_results(&f, DISABLED);
          break;
        }
        else if (!dep_already_probed) {
          // Switch to probe for the dependency feature instead
          if (verbose) {
            cout << "Feature dependency -- Prerequisite feature not probed for, switching to: " << f_dep.get_name() << endl;
          }
          f = f_dep;
          continue;
        }
        break;
      }
      if (dep_failed_disabled) {
        continue;
      }
      // Ready to probe
      f.probe(*this, verbose);
      // To use all availble probes just comment out the lines below
      most_likely = get_most_likely_version();
       if (most_likely.size() == 1) {
        cerr << "Found a single most likely version, stopping!" << endl;
         break;
      }
    }
  }
  catch (server_error& server_e) {
    if ( strncmp(server_e.what(), "DONE", 4) == 0 ) {
      // Fingerprinting done, no more probes available
    }
    else
      throw server_error(server_e.what()); // rethrow
  }
  
  cout << "*** Fingerprinting of server completed" << endl << endl;
  
  // Log the results if not in NaCL
  if (! nacl)
    log();
}

// Initialise the version_results vector
void Server::init_version_results() {
  vector<OPENSSL_VERSION> versions = unserialise_versions(CURRENT_VERSIONS);
  for (vector<OPENSSL_VERSION>::iterator it = versions.begin();
       it != versions.end();
       ++it) {
    version_result_t vres = {(*it), 500}; // {OPENSSL_VERSION, score}
    version_results_.push_back(vres);
  }
}


// Check if specified host:port is open using nmap
// First check if nmap is available
// which nmap | grep nmap
// nmap -p PORT guest-ubuntu | grep PORT | grep open
bool Server::port_open() const {
  int sys_ret=system("which nmap | grep nmap > .tmp");
  sys_ret = WEXITSTATUS(sys_ret);
  if (sys_ret != 0) {
    // nmap is not available
    cout << "nmap is not available on this system. Assuming port is open." << endl;
    return 1; // say port is open and hope for the best...
  }

  stringstream ss;
  ss << "nmap -p " << port_ << " " << host_ << " | grep " << port_ << " | grep open > .tmp";
  sys_ret=system(ss.str().c_str());
  sys_ret = WEXITSTATUS(sys_ret);
  
  // Check if server is running DTLS
  if (sys_ret != 0) {
    int dtls_ret = 0;
    stringstream base_command;
    base_command << "(echo Q) |  openssl s_client -connect " <<
    get_host() << ":" << get_port();
    string command;

    // DTLS 1.0
    command = base_command.str() +  " -dtls1 > /dev/null 2>&1";
    dtls_ret = system(command.c_str());
    dtls_ret = WEXITSTATUS(dtls_ret);
    if (dtls_ret == 1) {
      cerr << "DTLS is not supported on server: " << get_host() << ":" << get_port()  << endl;
    }
    else {
      cerr << "Server: " << get_host() << ":" << get_port()  <<
      " is running DTLS. This tool is intended for SSL/TLS." << endl;
    }
  }
  
  return (sys_ret == 0);
}
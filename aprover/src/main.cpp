// 
//
//  main.cpp
//  aprover
//
//  Created by Martin Andersson on 28/05/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Main functions for the command line tool.
 */

#include <vector>
#include <string>
#include <iostream>
#include <dirent.h>

#include "classes.h"
#include "version.h"
#include "database.h"
#include "feature.h"
#include "probe.h"
#include "server.h"
#include "c_probe_utils.h"
#include "parse_cmd_line.h"

using namespace std;

char* JSON_DIR;

// Probe for a certain feature
void probe_for_feature(Feature& f, const string& host, int lower_port, int upper_port, bool verbose) {
  stringstream summary;
  stringstream log_stream;
  
  for (int port = lower_port; port <= upper_port; ++port) {
    Server s(host, port);
    // Check so that port is open on the host
    if (!s.port_open()) {
      stringstream ss;
      ss << "Error: Port " << s.get_port() <<
      " is not open for SSL/TLS on host " << s.get_host() << endl;
      cerr << ss.str();
      summary << ss.str();
      continue;
    }
    // Check for support of protocol and extensions first
    // since some features rely on this information for a succesful probe
    Feature support = Database::search_name("TLS 1.1 or 1.2 support", false);
    support.probe(s, verbose);
    support = Database::search_name("Heartbeat extension support", false);
    support.probe(s, verbose);
    
    f.probe(s, verbose);
    summary << "Port " << s.get_port() << ": ";
    summary << status_as_string(s.get_probe_results()[2].status) << endl; // 0 is protocol and 1 is heartbeat
    summary << s.get_info() << endl;
  }
  
  log_stream << "////////////////////////////" << endl;
  log_stream << "/////// Info ///////////////" << endl;
  log_stream << "////////////////////////////" << endl;
  f.print(log_stream);
  log_stream << "////////////////////////////" << endl;
  log_stream << "/////// Results ////////////" << endl;
  log_stream << "////////////////////////////" << endl;
  
  log_stream << summary.str();
  cout << log_stream.str();
  
  string logfile = replace_invalid_file_characters(f.get_name() + ".log");
  // Put log files in separate directory
  logfile = LOG_DIR + logfile;
  
  int mkdir_status = mkdir(LOG_DIR.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  if (mkdir_status != 0 && errno != EEXIST)
  throw format_error("Could not create directory for log files.\n");
  
  ofstream log;
  log.open(logfile);
  log << log_stream.str();
  log.close();

}

// Fingerprint servers within given port range
void fingerprint_servers(const string& host,
                         int lower_port,
                         int upper_port,
                         bool stack, bool verbose) {
  for (int port = lower_port; port <= upper_port; ++port) {
    Server s(host, port);
    // Check so that port is open on the host
    if (!s.port_open()) {
      stringstream ss;
      ss << "Error: Port " << s.get_port() <<
      " is not open for SSL/TLS on host " << s.get_host() << endl << endl;;
      cerr << ss.str();
      continue;
    }
    
    // Perform stack test if flags are set
    Feature f = Database::search_name("stack test", false);
    if (stack) {
      f.probe(s, verbose);
      if (s.get_probe_results()[0].status != NOT_AFFECTED) {
        // Not running OpenSSL
        cout << "############################" << endl;
        cout << "The server at " << s.get_host() << " does not appear to be running OpenSSL" << endl;
        s.log();
        return;
      }
    }
    else
      s.add_probe_results(&f, DISABLED);

    s.fingerprint(verbose);
    s.print_verbose();
  }
}

// Checks so that the 'openssl' command points to
// version 1.0.2 or higher
void check_openssl_102_installed() {
  int sys_ret = -1;
  cout << "Locally installed OpenSSL version: " << flush;

  sys_ret = system("openssl version | grep 'OpenSSL 1.0.2'");
  sys_ret = WEXITSTATUS(sys_ret);
  
  if (sys_ret != 0) {
    cerr << format_string_for_output("Error: The tool is depending on that the command 'openssl' points to an OpenSSL binary of version 1.0.2 or higher. Exiting with error code 42") << endl;
    exit(42);
  }
  
}

// MAIN
int main(int argc, const char * argv[]) {
  try {
    string host;
    int lower_port;
    int upper_port;
    string feature_input;
    bool verbose;
    bool list;
    bool admin = false;
    bool update;
    bool stack;
    bool stack_only;
    
    // Parse the command line arguments
    parse_command_line(argc, argv,
                       host, lower_port, upper_port,
                       feature_input, verbose, list,
                       admin, update, stack, stack_only);

    string db_file = "aprover.db";
    char dir[16] = "json";
    JSON_DIR = &dir[0];
    
    // Only allow unobtrusive probing techniques if utilised on public website
    if (!admin) {
      Database::set_max_destructive(2);
      char dir[16] = "json_web";
      JSON_DIR = &dir[0];
      db_file = "aprover_web.db";
    }
    
    // Open database and update features
    Database::open(db_file.c_str());
    Database::update_from_json();
    
    if (list) {
      Database::list_all_features();
      return 0;
    }

    if (update) {
      Database::close();
      cout << "The database has been updated and written to file: " << db_file << endl;
      return 0;
    }
    
    if (host.empty()) {
      cout << "A host must be specified with the --host flag.\n";
      cout << "The --help flag details all the options available\n";
      return 0;
    }
    
    // Check OpenSSL 1.0.2 installed
    check_openssl_102_installed();
    
    if (stack_only) {
      feature_input = "stack test";
    }
    
    // If given a feature probe for that, otherwise try to fingerprint server
    if (!feature_input.empty()) {
      Feature f = Database::search_name(feature_input, false);
      probe_for_feature(f, host, lower_port, upper_port, verbose);
    }
    else {
      fingerprint_servers(host, lower_port, upper_port, stack, verbose);
    }
    
    Database::close();
  }
  catch (exception& e) {
    cerr << e.what() << endl;;
    Database::close();
    return 1;
  }
  
  return 0;
}

#!/bin/bash
#
# If no arguments are given, this script will start 
# a java SSL server at the default port 7433
# The server should already be compiled and named SSL_echoserver
# 
# Options are:
# -d : Enable debug
# -p : Specify starting port to use. Default is 7433. 
#      Needs to be given before the -v flag.


set -e # Exit on first error

DEBUG_FLAG=false
PORT=7443

while getopts ":p:d" opt; do
  case $opt in
    d)
      DEBUG_FLAG=true
      ;;
    p)
      PORT=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

srv="SSL_echoserver"
keystore="srvKeystore"
pwd="123456"

DEBUG_OPT=""
if $DEBUG_FLAG; then
  echo "Debug options enabled"
  DEBUG_OPT="-Djava.protocol.handler.pkgs=com.sun.net.ssl.internal.www.protocol -Djavax.net.debug=ssl"
fi

java -Djavax.net.ssl.keyStore=$keystore -Djavax.net.ssl.keyStorePassword=$pwd $DEBUG_OPT $srv $PORT


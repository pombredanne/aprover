//
//  c_probe_utils.h
//  aprover
//
//  Created by Martin Andersson on 24/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Declares useful functions for probes implemented in C and related constants.
 */


#ifndef c_probe_utils_h
#define c_probe_utils_h

// Disables assert
#ifndef NDEBUG
  #define NDEBUG 
#endif

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <openssl/ssl.h>
#include <openssl/bio.h>
#include <openssl/err.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <time.h>
#include <openssl/rand.h>

#include "ssl_record_constants.h"

// my_tls1_heartbeat
#include <openssl/objects.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <openssl/ocsp.h>

// Enables verbose output
#define UTILS_VERBOSE 0

#define BUF_SIZE 16386 // 16 k
#define READ_TIMEOUT 3 // in seconds

// Error code
#define EXIT_HANDSHAKE_FAILURE  6
#define EXIT_SSL_RECORD_FAILURE 7
#define EXIT_CONNECT_FAILURE    8
#define EXIT_LOOKUP_FAILURE     9
#define EXIT_UNKNOWN_FAILURE    10

// Struct to hold information about an SSL record
struct SSL_record_info {
  // Header
  unsigned char type;
  unsigned char minor_version;
  long data_size;
  
  // Data
  unsigned char subtype;
  unsigned char subsubtype;
};

// Prints buffer in hex format
void print_hex(const unsigned char* buffer, size_t bytes);

// Prints information about SSL_record_info
void print_SSL_record_info(struct SSL_record_info record);

// Reads an SSL record from given socket and
// returns SSL record information
struct SSL_record_info read_SSL_record(const int socket);

// Lookup IP address for given host
// This function is based on the example from the man-pages
// http://man7.org/linux/man-pages/man3/getaddrinfo.3.html
in_addr_t lookup_host(const char* host, int port);

// Set up a TCP socket
int setup_TCP_socket(int* sock, struct sockaddr_in* server_addr, const char* host, const int port);

// Send a client_hello
void send_clienthello(const int sock);

// Send close notify if no alert received
void send_close_notify(const int sock, struct SSL_record_info record);

// Raw data for the clienthello
extern unsigned char clienthello_data[343];
extern unsigned char clienthello_data2[305];
extern unsigned char clienthello_header[5];
extern const size_t clienthello_data_size;
extern const size_t clienthello_header_size;
extern const size_t clienthello_data2_size;
extern const size_t before_data2_size;
extern unsigned char close_notify[7];
extern const size_t close_notify_size;
extern const int server_minor_position;

/****************************/
// Used for BIO connections
// From  <ssl/ssl_locl.h>
#define s2n(s,c)    ((c[0]=(unsigned char)(((s)>> 8)&0xff), \
c[1]=(unsigned char)(((s)    )&0xff)),c+=2)

// Defined in the SSL library
int ssl3_write_bytes(SSL *s, int type, const void *buf, int len);

// use (ssl, 0,0,0) for default arguments
int my_tls1_heartbeat(SSL *s, unsigned int, unsigned int, unsigned int);


void init_openssl_library(void);
void print_ssl_error_string(unsigned long err, const char* const label);


#endif

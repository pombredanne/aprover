//
//  probe_fallback_scsv.c
//
//
//  Created by Martin Andersson on 08/07/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Probes for support of TLS_FALLBACK_SCSV as defined in RFC7507
 * Introducing the TLS_FALLBACK_SCSV thwarts attacks such as CVE-2014-3566 
 * a.k.a POODLE (downgrade to SSL3)
 *
 * Returns 0 if server does support TLS_FALLBACK_SCSV
 * Returns 1 if server does not support TLS_FALLBACK_SCSV
 * Returns 2 if downgrade not possible due to lacking protocol support (SSLv3).
 * Higher return codes are resevered for errors. See c_probe_utils.h.
 */

#include "c_probe_utils.h"

#define EXIT_SUPPORT 0 // not vulnerable
#define EXIT_NO_SUPPORT 1 // vulnerable
#define EXIT_NO_DOWNGRADE 2 // lower protocol not supported


// Probes the server for the feature
// See description at top of file for return codes used.
int c_probe_fallback_scsv(const char* host, int port, unsigned char lowest_protocol, int verbose)
{
  if (verbose) printf("lowest: 0x%02x\n", lowest_protocol);
  int ret = -1; // return value
  int     err;

  // For the socket
  int     sock;
  struct sockaddr_in server_addr;
  
  // Set up a TCP socket
  err = setup_TCP_socket(&sock, &server_addr, host, port);
  if (err == -1) {
    printf("errno: %d\n", errno);
    printf("probe_fallback_scsv -- ERROR: Connect of TCP socket failed.\n");
    return EXIT_CONNECT_FAILURE;
  }
  
  // Get random bytes
  unsigned char* rand_bytes = (unsigned char*)malloc(sizeof(unsigned char)*28);
  RAND_bytes(rand_bytes, 28);
  
  // Get seconds since epoch
  time_t rawtime = time(NULL); // now
  
  unsigned char* epoch_time = (unsigned char*)malloc(sizeof(unsigned char)*4);
  //unsigned long n = rawtime;
  epoch_time[0] = (rawtime >> 24) & 0xFF;
  epoch_time[1] = (rawtime >> 16) & 0xFF;
  epoch_time[2] = (rawtime >> 8) & 0xFF;
  epoch_time[3] = rawtime & 0xFF;
  
  // Debug info
  if (verbose) {
    printf("## INFO: rand bytes");
    print_hex(rand_bytes, 28);
    
    printf("\n## INFO: epoch time (%ju sec)", rawtime);
    print_hex(epoch_time, 4);
    printf("\n");
  }
  
  // Construct clienthello_data[343] as
  // [ clienthello_data[6], epoch_time[4], rand_bytes[28], clienthello_data2[305] ]
  memcpy(&clienthello_data[6], epoch_time, 4);
  memcpy(&clienthello_data[10], rand_bytes, 28);
  memcpy(&clienthello_data[38], clienthello_data2, clienthello_data2_size);
  
  /**********************************************/
  /* Client hello constructed and ready for use */
  /**********************************************/
  
  // Store original values
  unsigned char* old_values = (unsigned char*)malloc(sizeof(unsigned char)*2);
  memcpy(old_values, &clienthello_data2[219], 2);
  
  // Set a cipher suite to be TLS_FALLBACK_SCSV i.e 0x56 0x00
  clienthello_data[219] = 0x56;
  clienthello_data[220] = 0x00;
  
  // Set client version to be lowest protocol supported by the server
  clienthello_data[5] = lowest_protocol;
  
  
  if (verbose) {
    printf("probe_fallback_scsv -- Sending clienthello\n");
    //print_hex(clienthello_data, clienthello_data_size);
  }

  // Write client_hello
  write(sock, clienthello_header, clienthello_header_size);
  write(sock, clienthello_data, clienthello_data_size);
  
  if (verbose) printf("probe_fallback_scsv -- \
INFO: Waiting for reply.\n");
  
  // Servers with support for TLS_FALLBACK_SCSV will respond with
  // SSL_ALERT_INAPPROPRIATE_FALLBACK.
  // Servers without support should respond with ServerHello
  struct SSL_record_info record;
  record = read_SSL_record(sock);
  if (record.data_size > 0 &&
      record.type == SSL_RECORD_TYPE__ALERT &&
      record.subsubtype == SSL_ALERT_INAPPROPRIATE_FALLBACK ) {
    // Server responded with alert
    if (verbose) printf("probe_fallback_scsv -- \
INFO: Unffected server responded with expected alert: 0x%02x\n",
                                       record.subsubtype);
    ret = EXIT_SUPPORT;
  } else if (record.data_size > 0 &&
             record.type == SSL_RECORD_TYPE__HANDSHAKE &&
             record.subtype == SSL_HANDSHAKE_TYPE__SERVER_HELLO) {
    if (verbose) printf("probe_fallback_scsv -- \
INFO: Affected server responded with expected server_hello\n");
    ret = EXIT_NO_SUPPORT;
  } else if (record.data_size > 0 &&
             record.type == SSL_RECORD_TYPE__ALERT &&
             record.subsubtype == SSL_ALERT_HANDSHAKE_FAILURE) {
    if (verbose) printf("probe_fallback_scsv -- \
INFO: Downgrade not relevant, server responded with handshake_failure\n");
    ret = EXIT_NO_DOWNGRADE;
  } else {
    // Server did not respond with expected alert or server_hello
    if (verbose) printf("probe_fallback_scsv -- \
INFO: Server did not respond with INAPPROPRIATE_FALLBACK alert, \
HANDSHAKE_FAILURE alert or server_hello\n");
    ret = EXIT_UNKNOWN_FAILURE;
  }
  
  if (verbose || ret == EXIT_UNKNOWN_FAILURE) {
    printf("probe_fallback_scsv -- received following record:\n");
    print_SSL_record_info(record);
  }
  
  send_close_notify(sock, record);
  
  // Reset payload length and client minor version
  memcpy(&clienthello_data2[219], old_values, 2);
  clienthello_data[5] = SSL_MINOR_VERSION__TLS1_2;
  
  free(old_values);
  free(rand_bytes);
  free(epoch_time);
  
  return ret;
}

//
//  database.cpp
//  aprover
//
//  Created by Martin Andersson on 01/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * See database.h for an explanation of the different values stored
 * in the database.
 */

#include "database.h"

using namespace std;


// Initalize the database pointer
sqlite3* Database::db;

// Init max_destrucitve
int Database::max_destructive_ = 3;


// Opens database with name specified in filename
bool Database::open(const char* filename) {
  db = NULL;
  
  //if (sqlite3_open(filename, &db) == SQLITE_OK) {
  if (sqlite3_open_v2(filename, &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_NOMUTEX, "unix-none") == SQLITE_OK) {
    init_db();
    return true;
  }
  else {
    ostringstream oss;
    oss << "Database::open -- Could not open database with name: " << filename << endl;
    string ewhat = oss.str();
    throw database_error(ewhat);
    return false;
  }
}


// Closes the connection to the database
void Database::close() {
  sqlite3_close_v2(db);
}


// Initializes the database if not already done.
void Database::init_db() {
  sqlite3_exec(db,
               "CREATE TABLE IF NOT EXISTS Features ("
               "Name TEXT NOT NULL PRIMARY KEY,"
               "Description TEXT,"
               "Affected_list TEXT NOT NULL,"
               "Access_complexity INT NOT NULL,"
               "Destructive INT NOT NULL,"
               "Lib TEXT NOT NULL,"
               "Confidence INT,"
               "Dependency TEXT"
               ")", NULL, 0, NULL);
  check_for_error();
}


// Checks if the database has an error
void Database::check_for_error() {
  string error = sqlite3_errmsg(db);
  if(error != "not an error")
    throw database_error(error);
}


// Asks the prepared statement to the database. Returns a vector with
// all the rows of the result, where each row consits of a vector with
// each column as an element.
string_table_t Database::ask(sqlite3_stmt* statement) {
  string_table_t results;
  int cols = sqlite3_column_count(statement);
  int result = 0;
  
  while(true) {
    result = sqlite3_step(statement);
    if(result == SQLITE_ROW) {
      vector<string> values;
      string val;
      
      for(int col = 0; col < cols; col++) {
        char * ptr = (char*)sqlite3_column_text(statement, col);
        if(ptr)
          val = ptr;
        else
          val = "NULL";
        values.push_back(val);
      }
      results.push_back(values);
    }
    else
      break;
  }
  sqlite3_finalize(statement);
  return results;
}

// Update/save a feature to the database.
void Database::feature_update(const Feature& f) {
  sqlite3_stmt* stmt;
  const char* query = "INSERT or REPLACE INTO Features "
  "(Name, Description, "
  "Affected_list, Access_complexity, Destructive, Lib, Confidence, Dependency) "
  "values (?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17)";
  
  if (sqlite3_prepare_v2(db, query, -1, &stmt, 0) == SQLITE_OK) {
    sqlite3_bind_text(stmt, 10, f.get_name().c_str(), -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt, 11, f.get_description().c_str(), -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt,12,
                      serialise_versions(f.get_affected_versions()).c_str(),
                      -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(stmt, 13, f.get_access_complexity());
    sqlite3_bind_int(stmt, 14, f.get_destructive());
    sqlite3_bind_text(stmt, 15, f.get_lib().c_str(), -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(stmt, 16, f.get_confidence());
        sqlite3_bind_text(stmt, 17, f.get_dependency().c_str(), -1, SQLITE_TRANSIENT);
    
    sqlite3_step(stmt);
    sqlite3_finalize(stmt);
  }
  else
    throw database_error("Database::feature_update -- Could not prepare statement\n");
  
  check_for_error();
}

// Remove a feature from the database.
void Database::feature_remove(const Feature& f) {
  sqlite3_stmt* stmt;
  const char* query = "DELETE FROM Features WHERE Name = ?1";
  
  if (sqlite3_prepare_v2(db, query, -1, &stmt, 0) == SQLITE_OK) {
    sqlite3_bind_text(stmt, 1, f.get_name().c_str(), -1, SQLITE_TRANSIENT);
    sqlite3_step(stmt);
    sqlite3_finalize(stmt);
  }
  else
    throw database_error("Database::feature_remove -- Could not prepare statement\n");
  
  check_for_error();
}

// Update the database entries based on files in the json directory
void Database::update_from_json() {
  vector<string> jsons = json_in_dir(JSON_DIR);
  char file[100];
  for(vector<string>::const_iterator it = jsons.begin();
      it != jsons.end();
      ++it) {
    strcpy(file, JSON_DIR);
    strcat(file, "/");
    strcat(file, it->c_str());
    Database::feature_update(Feature(file));
  }
}

// Returns the first feature in the database with the given name
// The flag defines if to search for '*name*' or just 'name'
// Defaults to true
Feature Database::search_name(const string& name, bool regex) {
  sqlite3_stmt* stmt;
  string_table_t result;

  string name_like;
  if (regex)
    name_like = "%" + name + "%";
  else
    name_like = name;
  
  const char* query = "SELECT * FROM Features WHERE ( Destructive <= ?1 AND Name LIKE ?2 COLLATE NOCASE )";
  if (sqlite3_prepare_v2(db, query, -1, &stmt,0) == SQLITE_OK) {
    sqlite3_bind_int(stmt, 1, Database::get_max_destructive());
    sqlite3_bind_text(stmt, 2, name_like.c_str(), -1, SQLITE_TRANSIENT);
  }
  else
    throw database_error("Database::search_name -- Could not prepare statement\n");
  
  result = ask(stmt);
  check_for_error();
  
  if (result.size() == 0) {
    ostringstream oss;
    oss << "Database::search_name -- Did not find feature by name: " << name << endl;
    oss << "Use the --list flag to list features available to probe for." << endl;
    string ewhat = oss.str();
    throw database_error(ewhat);
  }

  int choice=0;
  // If several hits then ask user for which one
  if (result.size() > 1) {
    cout << "Found several features matching '*" << name << "*'" << endl;
    for(size_t i = 0; i<result.size(); ++i) {
      cout << "\tFeature " << i << ": " << result[i].at(0) << endl;
    }
    cout << "Please enter the number for the feature you wish to use: ";
    cin >> choice;
    while (cin.fail() || choice < 0 || (size_t)choice > (result.size()-1) ) {
      cin.clear();
      cin.ignore(numeric_limits<streamsize>::max(), '\n');
      cout << "Invalid option. Please enter a valid integer: ";
      cin >> choice;
    }
  }
  
  return database_row2feature(result[(size_t)choice]);
}

// Retrieve all features in the database ordered by Confidence that
// have not already been probed for.
// Return values are limited to the top 5 results
vector<Feature> Database::get_features_excluding_probed(vector<Feature>& probed) {
  vector<Feature> features;
  sqlite3_stmt* stmt;
  string_table_t result;
  
  int index = 0;
  stringstream ss;
  ss << "SELECT * FROM Features WHERE ( Destructive <= ?999 ";
  if (!probed.empty()) {
    for (size_t j = 0; j < probed.size() ; ++j) {
      ++index;
      ss << "AND Name != ?" << index << " ";
    }
  }
  ss << ") ";
  ss << "ORDER BY Destructive ASC LIMIT 1";
  
  // Bind names of already probed features if stmt prepared OK
  if (sqlite3_prepare_v2(db, ss.str().c_str(), -1, &stmt,0) == SQLITE_OK) {
    sqlite3_bind_int(stmt, 999, Database::get_max_destructive());
    for (size_t i = 1; i <= probed.size(); ++i)
      sqlite3_bind_text(stmt, (int)i, probed[i-1].get_name().c_str(), -1, SQLITE_TRANSIENT);
  }
  else
    throw database_error("Database::get_features_excluding_probed -- Could not prepare statement\n");
  
  result = ask(stmt);
  check_for_error();
  
  Feature f;
  // Iterate over rows in result
  for (size_t row = 0; row < result.size(); ++row) {
    f = database_row2feature(result[row]);
    features.push_back(f);
  }
  return features;
}

// Get features targeting given versions but exclude those already probed for
// Currently not in use in the main program.
// If you want to use it, Server::get_next_feature is the place to do so.
vector<Feature> Database::search_affected_excluding_probed(vector<OPENSSL_VERSION>& versions, std::vector<Feature>& probed) {
  vector<Feature> features;
  sqlite3_stmt* stmt;
  string_table_t result;
  
  int index = 0;
  stringstream ss;
  ss << "SELECT * FROM Features WHERE ( Destructive <= ?999 ";
  if (!probed.empty()) {
    for (size_t j = 0; j < probed.size() ; ++j) {
      ++index;
      ss << "AND Name != ?" << index << " ";
    }
  }
  ss << ") ";
  ss << "ORDER BY Destructive ASC LIMIT 1"; // 
  
  // Bind names of already probed features if stmt prepared OK
  if (sqlite3_prepare_v2(db, ss.str().c_str(), -1, &stmt,0) == SQLITE_OK) {
    sqlite3_bind_int(stmt, 999, Database::get_max_destructive());
    for (size_t i = 1; i <= probed.size(); ++i)
    sqlite3_bind_text(stmt, (int)i, probed[i-1].get_name().c_str(), -1, SQLITE_TRANSIENT);
  }
  else
  throw database_error("Database::search_affected_excluding_probed -- Could not prepare statement\n");
  
  result = ask(stmt);
  check_for_error();
  
  Feature f;
  // Iterate over rows in result
  for (size_t row = 0; row < result.size(); ++row) {
    f = database_row2feature(result[row]);
    // Iterate over versions that we want to be targeted
    for (size_t v = 0; v < versions.size(); ++v) {
      if (contains(f.get_affected_versions(), versions[v])) {
        features.push_back(f);
        break;
      }
    }
  }
  return features;
}

// Lists all features in ascending name order
void Database::list_all_features(ostream& stream) {
  sqlite3_stmt* stmt;
  string_table_t result;
  
  const char* query = "SELECT * FROM Features WHERE Destructive <= ?1 ORDER BY Name ASC";
  if (sqlite3_prepare_v2(db, query, -1, &stmt,0) == SQLITE_OK) {
    sqlite3_bind_int(stmt, 1, Database::get_max_destructive());
  }
  else {
    throw database_error("Database::list_all_features -- Could not prepare statement\n");
  }
  
  result = ask(stmt);
  check_for_error();
  
  stringstream summary;
  stream << "--- All features in database: " << endl;
  Feature f;
  // Iterate over rows in result
  for (size_t row = 0; row < result.size(); ++row) {
    f = database_row2feature(result[row]);
    summary << f.get_name() << endl;
    f.print(stream);
  }
  stream << "--- End of list ---" << endl;
  stream << "///////////////////" << endl;
  stream << "--- Summary -------" << endl;
  stream << summary.str();

}

// Converts a row in the Features database to a Feature object
Feature database_row2feature(vector<string> row) {
  if (row.size() != 8)
    throw database_error("database_row2feature -- Row is malformed.\n");
  
  Feature f(row[0],
            row[1],
            unserialise_versions(row[2]),
            atoi(row[3].c_str()),
            atoi(row[4].c_str()),
            row[5],
            atoi(row[6].c_str()),
            row[7]);
  return f;
}

// returns true if the container v contains the element x
bool contains(const std::vector<OPENSSL_VERSION>& v, const OPENSSL_VERSION& x) {
  bool ret = false;
  for(size_t i = 0; i < v.size(); ++i) {
    if (v[i] == x) {
      ret = true;
      break;
    }
  }
  return ret;
}

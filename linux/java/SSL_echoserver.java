/*
* A Java program that starts an SSL server and echoes any 
* payload input to the console.
*
* Takes as input an integer specifying the port to run the server on.
* Port defaults to 7443.
*
* Code taken from here: http://stilius.net/java/java_ssl.php
* Original author:Tomas Vilda
*/

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSession;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public
class SSL_echoserver {
    public static void
    main(String[] args) {
        int port = 7443;
        if (args.length > 0) {
            try {
                port = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                System.err.println("Argument" + args[0] + " must be an integer.");
                System.exit(1);
            }
        }
        run_server(port);   
        
    }

    public static void run_server(int port) {
        int count = 0;
        try {
            SSLServerSocketFactory sslserversocketfactory =
                    (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
            SSLServerSocket sslserversocket =
                    (SSLServerSocket) sslserversocketfactory.createServerSocket(port);

            SSLSocket sslsocket = (SSLSocket) sslserversocket.accept();
            
            while (count < 100) {
                count += 1;
                try {
                    InputStream inputstream = sslsocket.getInputStream();
                    InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
                    BufferedReader bufferedreader = new BufferedReader(inputstreamreader);

                    String string = null;
                    while ((string = bufferedreader.readLine()) != null) {
                        System.out.println(string);
                        System.out.flush();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("*** inner try caught exception");
                    sslsocket.close();
                    // For next loop
                    sslsocket = (SSLSocket) sslserversocket.accept();
                }
            }
            sslsocket.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("*** outer try caught an exception");
        }
        System.out.println("*** run_server:: return");
    }
}

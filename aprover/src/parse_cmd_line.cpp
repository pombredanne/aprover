//
//  parse_cmd_line.cpp
//  aprover
//
//  Created by Martin Andersson on 16/07/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Parse the command line and set relevant variables.
 * Library used to parse command line arguments is available from here:
 * http://tclap.sourceforge.net/
 */

#include "parse_cmd_line.h"

using namespace std;

// Parse the command line arguments
void parse_command_line(int argc, const char * argv[],
                        string& host, int& lower_port, int& upper_port,
                        string& feature_input, bool& verbose, bool& list, bool& admin,
                        bool& update, bool& stack, bool& stackOnly) {
  
  // Additianl parameters possible to assign from command line
  int port = 0;
  
  // Parse command line arguments using the library available from here:
  // http://tclap.sourceforge.net/
  try {
    string description = "Author: Martin Andersson";
    description +="\n"
    "This tool is intended to be used to fingerprint OpenSSL servers. "
    "The --openssl and --openssl-only flags can be used to give an indication if the targeted server seems to "
    "be running OpenSSL or not.\n"
    "The current version of the tool is focused on fingerprinting versions 1.0.1:1.0.1o and 1.0.2:1.0.2c but "
    "the current versions has support for the following range of versions: ";
    description += CURRENT_VERSIONS;
    description += "\nDISCLAIMER: The Author does not take any responsibility and can not be held liable in any way for any use of this software.";
    
    TCLAP::CmdLine cmd(description, ' ', "1.4 date: 2015-09-07");
    
    // Enable verbose output from probes
    TCLAP::SwitchArg verboseSwitch("","verbose","Enable verbose output.", false);
    cmd.add(verboseSwitch);
    
    // Only run stack test
    TCLAP::SwitchArg stackOnlySwitch("","openssl-only","The program will exit after having performed a test to see if the server seem to be using OpenSSL. Overrides --feature flag and --stack flag.", false);
    cmd.add(stackOnlySwitch);
    
    // Start with stack test
    TCLAP::SwitchArg stackSwitch("","openssl","Flag to indicate that the tool should start by running a test to see if the server seems to be using OpenSSL as its stack. If the server is not running OpenSSL the tool will abort.", false);
    cmd.add(stackSwitch);
    
    // Update db from json
    TCLAP::SwitchArg updateSwitch("","update","Update the database from the JSON files and then exit.", false);
    cmd.add(updateSwitch);
    
    
    // Run against web
    TCLAP::SwitchArg adminSwitch("","admin","Flag to indicate that the tool is to be run in administrator mode. Probing techniques are allowed to be obtrusive.", false);
    cmd.add(adminSwitch);
    
    // List available probes
    TCLAP::SwitchArg listSwitch("l","list","Lists all features in the database available to probe for and exits.", false);
    cmd.add(listSwitch);
    
    // Specific feature to probe for
    TCLAP::ValueArg<string> featureArg("f","feature",
                                       "Only probe for feature matching the given argument (not case sensitive). Use qoutes if argument contains whitespaces. The percentage sign (%) works as a wildcard.",
                                       false, "", "string", cmd);
    // Range of ports to test on
    TCLAP::ValueArg<string> rangeArg("r","range",
                                     "Port range of servers. Given as two integers separated with a colon.",
                                     false, "", "integer:integer", cmd);
    // Single port to test on
    TCLAP::ValueArg<int> portArg("p", "port",
                                 "Port of server, defaults to 443",
                                 false, 443, "integer", cmd);
    
    // Host address to test on
    TCLAP::ValueArg<string> hostArg("h", "host",
                                    "(required) The host address, for example 'facebook.com' or 'google.com' ",
                                    false, "", "string", cmd);
    // Parse the argv array.
    cmd.parse( argc, argv );
    
    // Need to init to default here because C++0x does not allow default init
    // and in Linux the values may be undefined if flag not set
    port = 0;
    lower_port = 0;
    upper_port = 0;
    stackOnly = false;
    stack = false;
    update = false;
    admin = false;
    list = false;
    verbose = false;
    
    // Get the value parsed by each arg.
    stackOnly = stackOnlySwitch.getValue();
    stack = stackSwitch.getValue();
    update = updateSwitch.getValue();
    admin = adminSwitch.getValue();
    list = listSwitch.getValue();
    verbose = verboseSwitch.getValue();
    feature_input = featureArg.getValue();
    string range = rangeArg.getValue();
    port = portArg.getValue();
    host = hostArg.getValue();
    
    // If port specified then it has to be greater than 0
    if (port != 0 && port < 0) {
      throw TCLAP::ArgException("Invalid port. Port must be a positive integer", "--port");
    }
    
    // Split range into lower and upper
    if (range.size()) {
      size_t delim = range.find(":");
      if (delim == string::npos)
        throw TCLAP::ArgException("Port range has to be delimited with a colon", "--range");
      try {
        lower_port = stoi(range.substr(0, delim));
        upper_port = stoi(range.substr(delim+1, string::npos));
      }
      catch (std::invalid_argument& e) {
        throw TCLAP::ArgException("Port range has to consist of two positive integers delimited with a colon", "--range");
      }
      if (upper_port < lower_port || upper_port <= 0)
        throw TCLAP::ArgException("Invalid port range", "--range");
    }
    
    // If range not used then set lower_port = upper_port = port
    if (lower_port == 0 && upper_port == 0) {
      lower_port = upper_port = port;
    }
    
  }
  catch (TCLAP::ArgException &e) {
    cerr << "Parsing error: " << e.error() << " " << e.argId() << endl;
    exit(1);
  }
  
}

//
//  probe_stack.cpp
//
//
//  Created by Martin Andersson on 01/07/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Probe to try and determine if the stack used is actually OpenSSL
 *
 * Loaded C library returns:
 *  0 if all tests conforms to the expected OpenSSL behaviour
 *  -1 if unknown failure
 * exit codes from c_probe_utils.h are available
 * exit codes > 100 specifies the number of tests that did not conform to
 *              the expected OpenSSL beahviour,
 *              e.g. return code 103 -> 3 tests did not conform
 */

#include "probe.h"
#include "ssl_record_constants.h"
#include <stdlib.h>
#include <stdio.h>

using namespace std;

class C_Probe_Stack : public Probe {
public:
  virtual int run(Server&, bool verbose);
private:
};

typedef int (*FUNC_PTR)(const char*, int, int);

int C_Probe_Stack::run(Server& s, bool verbose) {
  int ret = 0;
  this->set_status(FAILED);
  
  void *handle;
  int (*probe)(const char*, int, int);
  char *error;
  
  handle = dlopen("lib/probe_stack_c.so", RTLD_LAZY);
  if (!handle) {
    fputs (dlerror(), stderr);
    return 1;
  }
  if (verbose) cout << "Loaded C probe library probe_stack_c.so\n";
  
  probe = (FUNC_PTR)dlsym(handle, "c_stack_probe");
  if ((error = dlerror()) != NULL)  {
    fputs(error, stderr);
    return 1;
  }
  if (verbose) cout << "Loaded C probe symbol c_stack_probe.\n";
  
  int c_ret = probe(s.get_host().c_str(), s.get_port(), verbose);
  s.add_info("probe_stack -- ");
  
  if (c_ret == 0) {
    // Beahaves as an OpenSSL server is expected to do.
    this->set_status(NOT_AFFECTED);
    s.add_info("Beahaves as an OpenSSL server is expected to do.\n");
  }
  else if (c_ret > 100) {
    this->set_status(AFFECTED);
    stringstream ss;
    ss << c_ret-100 << " test(s) did not behave as expected for an OpenSSL server\n";
    s.add_info(ss.str());
  }
  else if (c_ret == -1) {
    s.add_info("Unknown error occured\n");
  }
  else if (c_ret == 6) {
    // Handshake failure
    ret = 1;
    s.add_info("Handshake missing or invalid, aborted.\n");
    throw probe_error("Handshake missing or invalid, aborted.\n");
  }
  else if (c_ret == 7) {
    // SSL record failure
    ret = 1;
    s.add_info("SSL record declared negative data size.\n");
    throw probe_error("SSL record declared negative data size.\n");
  }
  else if (c_ret == 8) {
    // Connect failure
    ret = 1;
    s.add_info("Set up or connect of TCP socket failed.\n");
    throw probe_error("Set up or connect of TCP socket failed.\n");
  }
  else if (c_ret == 9) {
    // Lookup failure
    ret = 1;
    s.add_info("Could not look up host.\n");
    throw probe_error("Could not look up host.\n");
  }
  else {
    ret = 1;
    s.add_info("Unknown return code from library.\n");
    throw probe_error("Unknown return code from library.\n");
  }
  
  return ret;
}


// the class factories
extern "C" Probe* create() {
  return new C_Probe_Stack;
}

extern "C" void destroy(Probe* p) {
  delete p;
}

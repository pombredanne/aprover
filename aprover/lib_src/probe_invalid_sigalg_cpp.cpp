//
//  probe_invalid_sigalg.cpp
//  aprover
//
//  Created by Martin Andersson on 13/07/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//


/*
 * Same probe as for CVE-2015-0291, runs after that one has been run.
 * Or if in web mode run after Alert tautology has been run.
 * If alert tautoloy has been run with status NOT_AFFECTED the destructiveness 
 * of this probe is 1.
 * Distinction is made between major versions [0.9.8, 1.0.0, 1.0.1] and [1.0.2].
 */

#include "classes.h"
#include "probe.h"
#include "database.h"
#include "feature.h"
#include "server.h"
#include <stdlib.h>

using namespace std;

class Probe_Invalid_Sigalg : public Probe {
public:
  virtual int run(Server&, bool verbose);
private:
};

typedef int (*FUNC_PTR)(const char*, int);

int Probe_Invalid_Sigalg::run(Server& s, bool verbose) {
  int ret = 0;
  this->set_status(FAILED);
  
  // Alert tautology has been run and status is NOT_AFFECTED
  // otherwise we might crash the server and thereby the destructive level of this
  // probe effectively rises to 3.
  bool ok_to_run = false;
  vector<probe_result_t> probe_res = s.get_probe_results();
  cout << "probe_res size: " << probe_res.size() << endl;
  for (vector<probe_result_t>::iterator it = probe_res.begin();
       it != probe_res.end();
       ++it) {
    if (it->feature->get_name() == "Alert tautology" &&
        it->status == NOT_AFFECTED) {
      if (verbose) cout << "probe_invalid_sigalg -- Alert tautology has status NOT_AFFECTED, ok to proceed\n";
      ok_to_run = true;
      break;
    }
  }
  
  // Check if CVE-2015-0291 has been run and if status was not affected,
  // in that case it's ok to proceed.
  if (!ok_to_run) {
    for (vector<probe_result_t>::iterator it = probe_res.begin();
         it != probe_res.end();
         ++it) {
      if (it->feature->get_name() == "CVE-2015-0291" &&
          it->status == NOT_AFFECTED) {
        if (verbose) cout << "probe_invalid_sigalg -- CVE-2015-0291 has status NOT_AFFECTED, ok to proceed\n";
        ok_to_run = true;
         break;
      }
    }
  }


  if (!ok_to_run) {
    string info = "probe_invalid_sigalg -- 'Alert tautology' or 'CVE-2105-0291' does not have status NOT_AFFECTED \
which means that the targeted server may be running version 1.0.2 which this probe would crash. \
The 'Invalid signature algorithm' probe is therefore disabled.\n";
    cout << info;
    s.add_info(info);
    this->set_status(DISABLED);
    return 0;
  }
  
  // alert_tautology not found so we are not in web mode
  // since the json in the plugin has a dependency on alert tautology.
  // ok to proceed.
  
  
  stringstream ss;
  ss << s.get_host() << ":" << s.get_port();
  string host_port = ss.str();
  
  void *handle;
  int (*probe)(const char*, int);
  char *error;
  
  handle = dlopen("lib/probe_CVE-2015-0291_c.so", RTLD_LAZY);
  if (!handle) {
    fputs (dlerror(), stderr);
    return 1;
  }
  
  probe = (FUNC_PTR)dlsym(handle, "c_probe_2015_0291");
  if ((error = dlerror()) != NULL)  {
    fputs(error, stderr);
    return 1;
  }
  
  int c_ret = probe(host_port.c_str(), verbose);
  s.add_info("probe_invalid_sigalg -- ");
  
  if (c_ret == 1) {
    // Vulnerable to CVE-2015-0291
    // Should never end up here because that probe should already have been run
    // and given a single most likely version
    cout << "Server vulnerable to CVE-2015-0291. Should never end up here because that probe should already have been run and given a single most likely version." << endl;
    s.add_info("Server vulnerable to CVE-2015-0291. Should never end up here because that probe should already have been run and given a single most likely version.\n");
    this->set_status(FAILED);
  }
  else if (c_ret == 0) {
    // Affected
    cout << "Server responded with handshake to an invalid signature algorithm" << endl;
    s.add_info("Server responded with handshake to an invalid signature algorithm\n");
    this->set_status(AFFECTED);
  }
  else if (c_ret == 2) {
    // Not Affected
    cout << "Server responded with alert to an invalid signature algorithm" << endl;
    s.add_info("Server responded with alert to an invalid signature algorithm\n");
    this->set_status(NOT_AFFECTED);
  }
  else if (c_ret == 9) {
    // Could not connect
    ret = 1;
    s.add_info("Could not connect to server. May be beacuse server is of version 1.0.2 and the probe for CVE-2015-0291 caused it to crash\n");
    throw probe_error("Could not connect to server. May be beacuse server is of version 1.0.2 and the probe for CVE-2015-0291 caused it to crash\n");
  }
  else if (c_ret == 10) {
    ret = 1;
    s.add_info("Received unexpected record.\n");
    throw probe_error("Received unexpected record.\n");
  }
  else if (c_ret == 20) {
    ret = 1;
    s.add_info("Valid renegotiation failed.\n");
    throw probe_error("Valid renegotiation failed.\n");
  }
  else {
    cerr << "probe_invalid_sigalg.cpp -- Unknown return code " << c_ret << endl;
    throw probe_error("Unknown return code from library\n");
  }
  
  return ret;
}

// the class factories
extern "C" Probe* create() {
  return new Probe_Invalid_Sigalg;
}

extern "C" void destroy(Probe* p) {
  delete p;
}


//
//  probe_TLS1_SHA256_c.c
//  aprover
//
//  Created by Martin Andersson on 21/07/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//


/*
 * Probes for support of TLS 1.0 and a SHA256 cipher suite in the same connection.
 * Only OpenSSL 1.0.1 (erroneously) supports such a connection.
 *
 * Connection setup is based on the script from the OpenSSL wiki page
 * https://wiki.openssl.org/index.php/SSL/TLS_Client
 */


#include "c_probe_utils.h"

// Probing technique using the OpenSSL C API
int c_probetls1_sha256(const char* host, int port, int verbose)
{
  if (verbose) printf("probe_TLS1_SHA256.c host in: %s, port %d\n", host, port);
  int ret = -1;
 
  char* host_port = (char*)malloc(sizeof(char)*128);
  strcat(host_port, host);
  char* port_str = (char*)malloc(sizeof(char)*16);
  strcat(host_port, port_str);

  if (verbose) printf("probe_TLS1_SHA256.c host_port %s\n", host_port);
  
  long res = 1;
  unsigned long ssl_err = 0;

  SSL_CTX* ctx = NULL;
  BIO *sbio = NULL, *out = NULL;
  
  SSL *ssl = NULL;
  
  // First test for SHA256 support
  do {
    // Internal function that wraps the OpenSSL init's  
    init_openssl_library();
    
    const SSL_METHOD* method = SSLv23_method();
    
    ssl_err = ERR_get_error();
    assert(NULL != method);
    if(!(NULL != method)) {
      print_ssl_error_string(ssl_err, "SSLv23_method");
      break; // failed
    }
    
    ctx = SSL_CTX_new(method);
    ssl_err = ERR_get_error();
    assert(ctx != NULL);
    if(!(ctx != NULL)) {
      print_ssl_error_string(ssl_err, "SSL_CTX_new");
      break; // failed
    }
    
    // To remove SSLv2 & 3 use SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3
    // To remove compression use SSL_OP_NO_COMPRESSION
    const long flags = SSL_OP_ALL ;
    long old_opts = SSL_CTX_set_options(ctx, flags);
    (void)old_opts; // Unused variable
    
    // Only allow SHA256 ciphers
    res = SSL_CTX_set_cipher_list(ctx, "SHA256");
    assert(1 == res);
    if(!(1 == res)) {
      ret = EXIT_CONNECT_FAILURE;
      printf("--- SHA256 ciphers could not be selected\n");
      ret = 17;
      break; // failed
    }
    
    sbio = BIO_new_ssl_connect(ctx);
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_new_ssl_connect\n");
    assert(sbio != NULL);
    if(!(sbio != NULL)) {
      print_ssl_error_string(ssl_err, "BIO_new_ssl_connect");
      ret = EXIT_CONNECT_FAILURE;
      break; // failed
    }
    
    res = BIO_set_conn_hostname(sbio, host_port);
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_set_conn_hostname\n");
    assert(1 == res);
    if(!(1 == res)) {
      print_ssl_error_string(ssl_err, "BIO_set_conn_hostname");
      ret = EXIT_CONNECT_FAILURE;
      break; // failed
    }
    
   // This copies an internal pointer. No need to free.
    BIO_get_ssl(sbio, &ssl);
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_get_ssl\n");
    assert(ssl != NULL);
    if(!(ssl != NULL)) {
      print_ssl_error_string(ssl_err, "BIO_get_ssl");
      break; //
    }
    
    out = BIO_new_fp(stdout, BIO_NOCLOSE);
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_new_fp\n");
    if (verbose) fprintf(stderr,"--- ssl state: %s\n",SSL_state_string_long(ssl));
    assert(NULL != out);
    if(!(NULL != out)) {
      print_ssl_error_string(ssl_err, "BIO_new_fp");
      break; // failed
    }
    
    res = BIO_do_connect(sbio);
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_do_connect\n");
    if (verbose) fprintf(stderr,"--- ssl state: %s\n",SSL_state_string_long(ssl));
    if(!(1 == res)) {
      print_ssl_error_string(ssl_err, "BIO_do_connect");
      ret = EXIT_CONNECT_FAILURE;
      break; // failed
    }
    
    res = BIO_do_handshake(sbio);
    ssl_err = ERR_get_error();
    if (verbose) printf("--- BIO_do_handshake\n");
    if (verbose) fprintf(stderr,"--- ssl state: %s\n",SSL_state_string_long(ssl));
    assert(1 == res);
    if(!(1 == res)) {
      print_ssl_error_string(ssl_err, "BIO_do_handshake");
      ret = EXIT_HANDSHAKE_FAILURE;
      break; // failed
    }
    
    ret = 1; // SHA256 supported
    
  } while (0);
  
  // Free up memory and close down connection
  if(out)
    BIO_free(out);
  if(sbio != NULL)
    BIO_free_all(sbio);
  if(NULL != ctx)
    SSL_CTX_free(ctx);
  
  // Check for support of both TLS1 and SHA256
  // For the socket
  int     sock, err;
  struct sockaddr_in server_addr;
  
  // Set up a TCP socket
  err = setup_TCP_socket(&sock, &server_addr, host, port);
  if (err == -1) {
    printf("errno: %d\n", errno);
    printf("probe_fallback_scsv -- ERROR: Connect of TCP socket failed.\n");
    return EXIT_CONNECT_FAILURE;
  }
  
  unsigned char mod_header[5] = {
    SSL_RECORD_TYPE__HANDSHAKE,
    SSL_MAJOR_VERSION__SSL3_TLS,
    SSL_MINOR_VERSION__TLS1_0,
    0x00, 0x8a};
  
  unsigned char clienthello_mod[138] = {
    0x01, 0x00, 0x00, 0x86, 0x03, 0x01, 0x55, 0xaf, 0x5f, 0x22, 0x56, 0x6d, 0x06, 0xd9, 0x64, 0x6e,
    0x97, 0xf6, 0x71, 0x20, 0x1c, 0xeb, 0xda, 0xf8, 0x41, 0xdc, 0x79, 0x4f, 0xd8, 0x70, 0x4e, 0xae,
    0xf2, 0x4e, 0xc9, 0x78, 0x2f, 0x9f, 0x00, 0x00, 0x14, 0x00, 0x6b, 0x00, 0x6a, 0x00, 0x6d, 0x00,
    0x3d, 0x00, 0x67, 0x00, 0x40, 0x00, 0x6c, 0x00, 0x3c, 0x00, 0x3b, 0x00, 0xff, 0x01, 0x00, 0x00,
    0x49, 0x00, 0x0b, 0x00, 0x04, 0x03, 0x00, 0x01, 0x02, 0x00, 0x0a, 0x00, 0x34, 0x00, 0x32, 0x00,
    0x0e, 0x00, 0x0d, 0x00, 0x19, 0x00, 0x0b, 0x00, 0x0c, 0x00, 0x18, 0x00, 0x09, 0x00, 0x0a, 0x00,
    0x16, 0x00, 0x17, 0x00, 0x08, 0x00, 0x06, 0x00, 0x07, 0x00, 0x14, 0x00, 0x15, 0x00, 0x04, 0x00,
    0x05, 0x00, 0x12, 0x00, 0x13, 0x00, 0x01, 0x00, 0x02, 0x00, 0x03, 0x00, 0x0f, 0x00, 0x10, 0x00,
    0x11, 0x00, 0x23, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x01, 0x01
  };
  
  // Write client_hello
  write(sock, mod_header, 5);
  write(sock, clienthello_mod, 138);
  
  if (verbose) printf("probe_TLS1_SHA256 -- INFO: Waiting for reply.\n");
  
  // Servers that support TLS1 and SHA256 should respond with ServerHello
  // others should respond with HANDSHAKE_FAILURE
  struct SSL_record_info record;
  record = read_SSL_record(sock);
  if (record.data_size > 0 &&
      record.type == SSL_RECORD_TYPE__ALERT &&
      record.subsubtype == SSL_ALERT_HANDSHAKE_FAILURE ) {
    // Server responded with alert
    if (verbose) printf("probe_TLS1_SHA256 -- \
INFO: Unffected server responded with expected alert: 0x%02x\n",
                        record.subsubtype);
    ret = 0; // Not affected
  } else if (record.data_size > 0 &&
             record.type == SSL_RECORD_TYPE__HANDSHAKE &&
             record.subtype == SSL_HANDSHAKE_TYPE__SERVER_HELLO) {
    if (verbose) printf("probe_TLS1_SHA256 -- \
INFO: Affected server responded with expected server_hello\n");
    ret = 1; // Affected
  } else {
    // Server did not respond with expected alert or server_hello
    if (verbose) printf("probe_TLS1_SHA256 -- \
INFO: Server did not respond with HANDSHAKE_FAILURE alert or server_hello\n");
    ret = EXIT_UNKNOWN_FAILURE;
  }
  
  if (verbose || ret == EXIT_UNKNOWN_FAILURE) {
    printf("probe_TLS1_SHA256 -- received following record:\n");
    print_SSL_record_info(record);
  }
  
  send_close_notify(sock, record);
  
  return ret;
}

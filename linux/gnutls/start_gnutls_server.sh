#!/bin/bash
#
# If no arguments are given, this script will start 
# a gnutls SSL server at the default port 8433
# The server should already be compiled and named SSL_echoserver
# 
# Options are:
# -d : Enable debug
# -p : Specify starting port to use. Default is 8443. 
#      Needs to be given before the -v flag.


set -e # Exit on first error

DEBUG_FLAG=false
PORT=8443

while getopts ":p:d" opt; do
  case $opt in
    d)
      DEBUG_FLAG=true
      ;;
    p)
      PORT=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

srv="gnutls-serv"
cafile="x509-ca.pem"
keyfile="x509-server-key.pem"
certfile="x509-server.pem"

DEBUG_OPT="1"
if $DEBUG_FLAG; then
  echo "Debug options enabled"
  DEBUG_OPT="4"
fi

gnutls-serv --echo --x509cafile $cafile --x509keyfile $keyfile --x509certfile $certfile --port $PORT --debug $DEBUG_OPT


//
//  classes.h
//  aprover
//
//  Created by Martin Andersson on 12/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

#ifndef aprover_classes_h
#define aprover_classes_h

// Declaration of classes needed because some classes
// include each other which otherwise creates a circular dependency.

#include <vector>
#include <algorithm>

class Server;
class Feature;
class Probe;
class Database;

// Template function returning true if the container v contains the element x
// Only works in C++11 so for the plugin to compile (C++0x) specific versions
// are implemented where needed
/*
template<class C, class T>
auto contains(const C& v, const T& x)
-> decltype(end(v), true)
{
  return end(v) != std::find(begin(v), end(v), x);
}
*/

#endif

//
//  probe_CCS.c
//
//
//  Created by Martin Andersson on 18/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Probes the given host and port for the early CCS vulnerability.
 * The main function c_ccs_probe(host, port) is loaded by external
 * cpp file and run from there.
 * Returns 0 if server is not vulnereable to early CCS.
 * Returns 1 if server is vulnereable to early CCS.
 * Higher return codes are resevered for errors. See c_probe_utils.h.
 */

#include "c_probe_utils.h"

// Return codes
#define EXIT_NO_CCS             0
#define EXIT_CCS                1


// Probes the server for the feature
// See description at top of file for return codes used.
int c_ccs_probe(const char* host, int port, int verbose)
{
  int ccs = 0; // Flag to indicate CCS vulnerability
  int     err;
  
  int     sock;
  struct sockaddr_in server_addr;
  /* ----------------------------------------------- */
  // Set up a TCP socket
  
  err = setup_TCP_socket(&sock, &server_addr, host, port);
  
  if (err == -1) {
    printf("errno: %d\n", errno);
    printf("probe_CCS -- ERROR: Connect of TCP socket failed.\n");
    return EXIT_CONNECT_FAILURE;
  }
  
  /* ----------------------------------------------- */
  // Perform SSL Handshake
  
  send_clienthello(sock);
  // Init to invalid value, the value is set further down
  unsigned char server_minor_version = SSL_MINOR_VERSION__UNKNOWN;
  
  // Server should respond with server_hello, certificate, server_key_exchange, server_done
  int server_hello_received = 0;
  int certificate_received = 0;
  struct SSL_record_info record;
  do {
    record = read_SSL_record(sock);
    if (record.data_size <= 0) {
      ccs = EXIT_SSL_RECORD_FAILURE;
      goto shutdown;
    }
    if (record.data_size > 0 && record.type == SSL_RECORD_TYPE__HANDSHAKE) {
      if (record.subtype == SSL_HANDSHAKE_TYPE__SERVER_HELLO) {
        server_hello_received = 1;
        // Use the supported protocol in subsequent communication
        server_minor_version = record.minor_version;
         if (record.data_size > 800) break; // Handle several records sent in one packet
      }
      if (record.subtype == SSL_HANDSHAKE_TYPE__CERTIFICATE)
      certificate_received = 1;
    }
  } while (record.data_size > 0 &&
           record.type != SSL_RECORD_TYPE__ALERT &&
           (record.type == SSL_RECORD_TYPE__HANDSHAKE &&
            record.subtype != SSL_HANDSHAKE_TYPE__SERVER_DONE) );
  
  if (!server_hello_received || !certificate_received) {
    printf("probe_CCS -- ERROR errno %s: Handshake missing or invalid, Aborting.\n", strerror(errno));
    ccs = EXIT_HANDSHAKE_FAILURE;
    goto shutdown;
  }
  
  // Last record received should have been a handshake record
  if (record.data_size > 0 && record.type == SSL_RECORD_TYPE__HANDSHAKE) {
    if (verbose) printf("probe_CCS -- INFO: sending early CCS\n");
    
    // Send an early CCS
    unsigned char CCS_header[5] = {SSL_RECORD_TYPE__CCS,
      SSL_MAJOR_VERSION__SSL3_TLS,
      server_minor_version,
      0x00, 0x01};
    unsigned char CCS_data[1] = {0x01};
    write(sock, CCS_header, 5);
    write(sock, CCS_data, 1);
    
    // Send close notify to force response from server
    // socket may already be closed by peer, ignore SIGPIPE
    struct sigaction new_actn, old_actn;
    new_actn.sa_handler = SIG_IGN;
    sigemptyset (&new_actn.sa_mask);
    new_actn.sa_flags = 0;
    sigaction (SIGPIPE, &new_actn, &old_actn);
    
    // send close notify
    memcpy(&close_notify[server_minor_position], &server_minor_version, 1);
    write(sock, close_notify, close_notify_size);
    
    // restore signal handling
    sigaction (SIGPIPE, &old_actn, NULL);
    
    record = read_SSL_record(sock);
    if (record.data_size <= 0) {
      ccs = EXIT_SSL_RECORD_FAILURE;
      goto shutdown;
    }
    if (record.data_size > 0 &&
         record.type == SSL_RECORD_TYPE__ALERT &&
         (record.subsubtype == SSL_ALERT_BAD_RECORD_MAC ||
          record.subsubtype == SSL_ALERT_DECRYPTION_FAILED) ) {
          // Server is vulnerable, 0.9.8 and 1.0.0 will respond with DECRYPTION_FAILED
          if (verbose) printf("probe_CCS -- INFO: Server is vulnerable to early CCS\n");
          ccs = EXIT_CCS;
        }
    else if (record.data_size > 0 &&
             record.type == SSL_RECORD_TYPE__ALERT &&
             record.subsubtype == SSL_ALERT_UNEXPECTED_MSG) {
      // Server not vulnerable
      if (verbose) printf("probe_CCS -- INFO: Server not vulnerable to early CCS\n");
      ccs = EXIT_NO_CCS;
    }
    else {
      printf("probe_CCS -- Server responded with the following unexpected record:");
      ccs = EXIT_UNKNOWN_FAILURE;
    }
    if (verbose || ccs == EXIT_UNKNOWN_FAILURE) {
      print_SSL_record_info(record);
    }
  }
  else {
    // Last message was not a handshake record
    printf("probe_CCS -- ERROR errno %s: Handshake missing or invalid.\n", strerror(errno));
    ccs =  EXIT_HANDSHAKE_FAILURE;
  }
  
  /* ----------------------------------------------- */
  // Close down connection
  send_close_notify(sock, record);
  
  // Shutdown connection in both directions
shutdown:
  close(sock);

  return ccs;
}
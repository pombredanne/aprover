//
//  probe_fallback_scsv.c
//
//
//  Created by Martin Andersson on 08/07/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Probes for support of TLS_FALLBACK_SCSV as defined in RFC7507
 * Introducing the TLS_FALLBACK_SCSV thwarts attacks such as CVE-2014-3566
 * a.k.a POODLE (downgrade to SSL3)
*/

#include "probe.h"
#include "ssl_record_constants.h"
#include <stdlib.h>
#include <stdio.h>

using namespace std;

class C_Probe_Fallback_SCSV : public Probe {
public:
  virtual int run(Server&, bool verbose);
private:
};

typedef int (*FUNC_PTR)(const char*, int, unsigned char, int);

int C_Probe_Fallback_SCSV::run(Server& s, bool verbose) {
  int ret = 0;
  this->set_status(FAILED);
  
  // Check if downgrade possible
  unsigned char highest_protocol = SSL_MINOR_VERSION__UNKNOWN;
  unsigned char lowest_protocol = SSL_MINOR_VERSION__UNKNOWN;
  if (s.TLSv1_2_support)
    highest_protocol = SSL_MINOR_VERSION__TLS1_2;
  else if (s.TLSv1_1_support)
    highest_protocol = SSL_MINOR_VERSION__TLS1_1;
  else if (s.TLSv1_0_support)
    highest_protocol = SSL_MINOR_VERSION__TLS1_0;
  else if (s.SSLv3_support)
    highest_protocol = SSL_MINOR_VERSION__SSL3;
  
  bool lower_found = false;
  if (s.SSLv3_support &&
      highest_protocol > SSL_MINOR_VERSION__SSL3) {
    lowest_protocol = SSL_MINOR_VERSION__SSL3;
    lower_found = true;
  }
  else if (s.TLSv1_0_support &&
           highest_protocol > SSL_MINOR_VERSION__TLS1_0) {
    lowest_protocol = SSL_MINOR_VERSION__TLS1_0;
    lower_found = true;
  }
  else if (s.TLSv1_1_support &&
           highest_protocol > SSL_MINOR_VERSION__TLS1_1) {
    lowest_protocol = SSL_MINOR_VERSION__TLS1_1;
    lower_found = true;
  }
  else if (s.TLSv1_2_support)
    lowest_protocol = SSL_MINOR_VERSION__TLS1_2;
  
  if (!lower_found) {
    cout << "probe_fallback_scsv -- TLS_FALLBACK_SCSV not relevant due to server only supporting one protocol version." << endl;
    s.add_info("probe_fallback_scsv -- TLS_FALLBACK_SCSV not relevant due to server only supporting one protocol version.\n");
    this->set_status(DISABLED);
    return 0;
  }
  
  void *handle;
  int (*probe)(const char*, int, unsigned char, int);
  char *error;
  
  handle = dlopen("lib/probe_fallback_scsv_c.so", RTLD_LAZY);
  if (!handle) {
    fputs (dlerror(), stderr);
    return 1;
  }
  
  probe = (FUNC_PTR)dlsym(handle, "c_probe_fallback_scsv");
  if ((error = dlerror()) != NULL)  {
    fputs(error, stderr);
    return 1;
  }
  
  int c_ret = probe(s.get_host().c_str(), s.get_port(), lowest_protocol, verbose);
  s.add_info("probe_fallback_scsv -- ");
  
  if (c_ret == 1) {
    cout << "TLS_FALLBACK_SCSV is not supported" << endl;
    s.add_info("TLS_FALLBACK_SCSV not supported\n");
    if ( lowest_protocol > SSL_MINOR_VERSION__SSL3 ) {
      cout << "However SSL3 not supported so not vulnerable to the POODLE attack." << endl;
      s.add_info("probe_fallback_scsv -- However SSL3 not supported so not vulnerable to POODLE attack.\n");
      
    }
     this->set_status(AFFECTED);
  }
  else if  (c_ret == 0) {
    cout << "TLS_FALLBACK_SCSV is supported" << endl;
    s.add_info("TLS_FALLBACK_SCSV is supported\n");
    this->set_status(NOT_AFFECTED);
  }
  else if  (c_ret == 2) {
    cout << "TLS_FALLBACK_SCSV support could not be determined because server \
did not support protocol to downgrade to." << endl;
    s.add_info("TLS_FALLBACK_SCSV support could not be determined because server \
did not support protocol to downgrade to.\n");
    this->set_status(FAILED);
    ret = 1;
  }
  else if (c_ret == 6) {
    // Handshake failure
    ret = 1;
    s.add_info("Handshake missing or invalid, aborted.\n");
    throw probe_error("Handshake missing or invalid, aborted.\n");
  }
  else if (c_ret == 7) {
    // SSL record failure
    ret = 1;
    s.add_info("SSL record declared negative data size.\n");
    throw probe_error("SSL record declared negative data size.\n");
  }
  else if (c_ret == 8) {
    // Connect failure
    ret = 1;
    s.add_info("Set up or connect of TCP socket failed.\n");
    throw probe_error("Set up or connect of TCP socket failed.\n");
  }
  else if (c_ret == 9) {
    // Lookup failure
    ret = 1;
    s.add_info("Could not look up host.\n");
    throw probe_error("Could not look up host.\n");
  }
  else {
    ret = 1;
    s.add_info("Unknown return code from library.\n");
    throw probe_error("Unknown return code from library.\n");
  }
  
  return ret;
}


// the class factories
extern "C" Probe* create() {
  return new C_Probe_Fallback_SCSV;
}

extern "C" void destroy(Probe* p) {
  delete p;
}

//
//  probe_alert_tautology.c
//
//
//  Created by Martin Andersson on 24/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Probes for the commit found here:
 * https://github.com/openssl/openssl/commit/11414f17d93ec04d2d056d5f3a87e964a7412431
 * It initiates a handshake and then sends a close notify with an invalid
 * version number set in the header. In affected versions this trigger sending
 * an alert back with the invalid version and SSL_ALERT_PROTOCOL_VERSION.
 * Newer versions do not send an alert back, they only close down the connection.
 * Loaded by probe_alert_tautology.cpp
 *
 * Affects version: 0.9.8:0.9.8zg, 1.0.0:1.0.0s, 1.0.1:1.0.1m, 1.0.2:1.0.2a
 * Note that versions after 0.9.8zg and 1.0.0s may or may not be affected by
 * this depending on if the commit is implemented in those branches as well.
 *
 * Returns 0 if server is not affected by the probe.
 * Returns 1 if server is affected by the probe.
 * Higher return codes are resevered for errors. See c_probe_utils.h.
 */

#include "c_probe_utils.h"

// Return codes
#define EXIT_NO_CL_ALERT   0
#define EXIT_CL_ALERT      1


// Probes the server for the feature
// See description at top of file for return codes used.
int c_probe_alert_tautology(const char* host, int port, int verbose)
{
  int ret = EXIT_UNKNOWN_FAILURE; // Flag to indicate changelog
  int     err;
  
  int     sock;
  struct sockaddr_in server_addr;
  /* ----------------------------------------------- */
  // Set up a TCP socket
  
  err = setup_TCP_socket(&sock, &server_addr, host, port);
  
  if (err == -1) {
    printf("errno: %d\n", errno);
    printf("probe_alert_tautology -- ERROR: Connect of TCP socket failed.\n");
    return EXIT_CONNECT_FAILURE;
    
  }
  
  /* ----------------------------------------------- */
  // Perform SSL Handshake
  
  send_clienthello(sock);
  // Init to invalid value, the value is set further down
  unsigned char server_minor_version = SSL_MINOR_VERSION__UNKNOWN;
  
  
  // Server should respond with server_hello, certificate, server_key_exchange, server_done
  int server_hello_received = 0;
  int certificate_received = 0;
  struct SSL_record_info record;
  do {
    record = read_SSL_record(sock);
    if (record.data_size <= 0) {
      ret = EXIT_SSL_RECORD_FAILURE;
      goto shutdown;
    }
    if (record.data_size > 0 && record.type == SSL_RECORD_TYPE__HANDSHAKE) {
      if (record.subtype == SSL_HANDSHAKE_TYPE__SERVER_HELLO) {
        server_hello_received = 1;
        // Use the supported protocol in subsequent communication
        server_minor_version = record.minor_version;
         if (record.data_size > 800) break; // Handle several records sent in one packet
      }
      if (record.subtype == SSL_HANDSHAKE_TYPE__CERTIFICATE)
        certificate_received = 1;
    }
  } while (record.data_size > 0 &&
           record.type != SSL_RECORD_TYPE__ALERT &&
           (record.type == SSL_RECORD_TYPE__HANDSHAKE &&
            record.subtype != SSL_HANDSHAKE_TYPE__SERVER_DONE) );
  
  if (verbose) printf("probe_alert_tautology -- Sending close notify with invalid version number.\n");
  
  printf("probe_alert_tautology -- Unaffected servers are expected to reset the connection\n");
   
  // Send a close notify with invalid version number
  unsigned char invalid_version = SSL_MINOR_VERSION__UNKNOWN;
  unsigned char alert_header[5] = {SSL_RECORD_TYPE__ALERT,
    SSL_MAJOR_VERSION__SSL3_TLS,
    invalid_version, // invalid version number to trigger change
    0x00, 0x02};
  unsigned char alert_data[2] = {SSL_ALERT_WARNING, SSL_ALERT_CLOSE_NOTIFY};
  
  write(sock, alert_header, 5);
  write(sock, alert_data, 2);
  
  record = read_SSL_record(sock);
  if (record.data_size > 0 &&
      record.minor_version == invalid_version &&
      record.type == SSL_RECORD_TYPE__ALERT &&
      record.subsubtype == SSL_ALERT_PROTOCOL_VERSION) {
    // We've managed to trigger the sending of an alert
    if (verbose) printf("probe_alert_tautology -- Alert received!\n");
    ret = EXIT_CL_ALERT;
  }
  else if (record.data_size == 0){
    if (verbose) printf("probe_alert_tautology -- Alert not received.\n");
    ret = EXIT_NO_CL_ALERT;
  }
  else {
    printf("probe_alert_tautology -- Server responded with the following unexpected record:");
    ret = EXIT_UNKNOWN_FAILURE;
  }
  if (verbose || ret == EXIT_UNKNOWN_FAILURE) {
    print_SSL_record_info(record);
  }
  
                          /* ----------------------------------------------- */
  // Close down connection. This is only an extra precaution
  // because the connection should already be closed due to the
  // alert sent.
  
  send_close_notify(sock, record);

    // Shutdown connection in both directions
shutdown:
  close(sock);
  
  return ret;
}
//
//  probe_protocol.cpp
//  aprover
//
//  Created by Martin Andersson on 11/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Probes for the SSL/TLS protocol supported with the use of
 * probe_protocol.c which uses the OpenSSL C API
 * Also gives information about other supported protocols.
 */

#include "probe.h"
#include <stdlib.h>

#include <stdio.h>
#include <stdlib.h>

using namespace std;

class C_Probe_Protocol : public Probe {
public:
  virtual int run(Server&, bool verbose);
private:
};

typedef int (*FUNC_PTR)(const char*, unsigned char, int);

int C_Probe_Protocol::run(Server& s, bool verbose) {
  int ret = 0;
  this->set_status(FAILED);
  
  stringstream ss;
  ss << s.get_host() << ":" << s.get_port();
  string host_port = ss.str();
  
  void *handle;
  int (*probe)(const char*, unsigned char, int);
  char *error;
  
  handle = dlopen("lib/probe_protocol_c.so", RTLD_LAZY);
  if (!handle) {
    fputs (dlerror(), stderr);
    return 1;
  }
  
  probe = (FUNC_PTR)dlsym(handle, "c_probe");
  if ((error = dlerror()) != NULL)  {
    fputs(error, stderr);
    return 1;
  }
  
  unsigned char protocol = SSL_MINOR_VERSION__UNKNOWN; // Allows all
  int c_ret = probe(host_port.c_str(), protocol, verbose);
  s.add_info("probe_protocol -- ");
  
  if (c_ret == 1) {
    cout << "Protocol: SSLv3" << endl;
    s.add_info("Protocol in use: SSLv3\n");
    s.set_minor_version(SSL_MINOR_VERSION__SSL3);
    s.SSLv3_support = true;
    this->set_status(NOT_AFFECTED);
  }
  else if  (c_ret == 2) {
    cout << "Protocol: TLSv1" << endl;
    s.add_info("Protocol in use: TLSv1.0\n");
    s.set_minor_version(SSL_MINOR_VERSION__TLS1_0);
    s.TLSv1_0_support = true;
    this->set_status(NOT_AFFECTED);
  }
  else if  (c_ret == 3) {
    cout << "Protocol: TLSv1.1" << endl;
    s.add_info("Protocol in use: TLSv1.1\n");
    s.set_minor_version(SSL_MINOR_VERSION__TLS1_1);
    s.TLSv1_1_support = true;
    this->set_status(AFFECTED);
  }
  else if  (c_ret == 4) {
    cout << "Protocol: TLSv1.2" << endl;
    s.add_info("Protocol in use: TLSv1.2\n");
    s.set_minor_version(SSL_MINOR_VERSION__TLS1_2);
    s.TLSv1_2_support = true;
    this->set_status(AFFECTED);
  }
  else if (c_ret == EXIT_HANDSHAKE_FAILURE) {
    // Handshake failure
    ret = 1;
    s.add_info("Handshake missing or invalid, aborted.\n");
    throw probe_error("Handshake missing or invalid, aborted.\n");
  }
  else if (c_ret == EXIT_CONNECT_FAILURE) {
    // Connect failure
    ret = 1;
    s.add_info("Set up or connect of TCP socket failed.\n");
    throw probe_error("Set up or connect of TCP socket failed.\n");
  }
  else if (c_ret == 0) {
    // Could not determine version
    s.add_info("Unknown protocol version\n");
    s.set_minor_version(SSL_MINOR_VERSION__UNKNOWN);
    ret = 1;
  }
  
  // Test protocols separately
  int prot_ret = 0;
  
  // TLS 1.2
  if (c_ret != 4) {
    protocol = SSL_MINOR_VERSION__TLS1_2;
    prot_ret = probe(host_port.c_str(), protocol, verbose);
    if (prot_ret != 4) {
      if (verbose) cout <<("probe_protocol -- TLS 1.2 not supported\n");
      s.add_info("probe_protocol -- TLS 1.2 not supported\n");
      s.TLSv1_2_support = false;
    }
    else {
      if (verbose) cout <<("probe_protocol -- TLS 1.2 supported\n");
      s.add_info("probe_protocol -- TLS 1.2 supported\n");
      s.TLSv1_2_support = true;
    }
  }
  // TLS 1.1
  if (c_ret != 3) {
    protocol = SSL_MINOR_VERSION__TLS1_1;
    prot_ret = probe(host_port.c_str(), protocol, verbose);
    if (prot_ret != 3) {
      if (verbose) cout <<("probe_protocol -- TLS 1.1 not supported\n");
      s.add_info("probe_protocol -- TLS 1.1 not supported\n");
      s.TLSv1_1_support = false;
    }
    else {
      if (verbose) cout <<("probe_protocol -- TLS 1.1 supported\n");
      s.add_info("probe_protocol -- TLS 1.1 supported\n");
      s.TLSv1_1_support = true;
    }
  }
  // TLS 1.0
  if (c_ret != 2) {
    protocol = SSL_MINOR_VERSION__TLS1_0;
    prot_ret = probe(host_port.c_str(), protocol, verbose);
    if (prot_ret != 2) {
      if (verbose) cout <<("probe_protocol -- TLS 1.0 not supported\n");
      s.add_info("probe_protocol -- TLS 1.0 not supported\n");
      s.TLSv1_0_support = false;
    }
    else {
      if (verbose) cout <<("probe_protocol -- TLS 1.0 supported\n");
      s.add_info("probe_protocol -- TLS 1.0 supported\n");
      s.TLSv1_0_support = true;
    }
  }
  // SSL 3
  if (c_ret != 1) {
    protocol = SSL_MINOR_VERSION__SSL3;
    prot_ret = probe(host_port.c_str(), protocol, verbose);
    if (prot_ret != 1) {
      if (verbose) cout <<("probe_protocol -- SSL3 not supported\n");
      s.add_info("probe_protocol -- SSL3 not supported\n");
      s.SSLv3_support = false;
    }
    else {
      if (verbose) cout <<("probe_protocol -- SSL3 supported\n");
      s.add_info("probe_protocol -- SSL3 supported\n");
      s.SSLv3_support = true;
    }
  }
  
  return ret;
}

// the class factories
extern "C" Probe* create() {
  return new C_Probe_Protocol;
}

extern "C" void destroy(Probe* p) {
  delete p;
}

//
//  probe_stack.c
//
//
//  Created by Martin Andersson on 01/07/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Probe to try and determine if the stack used is actually OpenSSL
 *
 * -- Test 0: Sets the payload length in the record header of the ClientHello message
 *    to 0xff 0xff. OpenSSL servers are expected to respond with a RECORD_OVERFLOW alert.
 * -- Test 1: Sets the message length in the ClientHello message to 0x00 0x00. OpenSSL 
 *    servers are expected to respond with a PROTOCOL_VERSION or DECODE_ERROR alert.
 * -- Test 2: Sets the message length in the ClientHello message to 0xff 0xff. 
 *    OpenSSL servers are expected to respond with a ILLEGAL_PARAMETER alert.
 * -- Test 3: Sets the compressions methods supported to 0x64 and 0x65 which are 
 *    unassigned according to IANA. OpenSSL servers are expected to respond with a DECODE_ERROR alert.
 * -- Test 4: Sets the payload length in the record header of the ClientKeyExchange
 *    message to 0x00 0x00. OpenSSL servers are expected to respond with a PROTOCOL_VERSION alert.
 * -- Test 5: Sets the cipher suites to only be 0xfe 0xee which is
 *    unassigned according to IANA. OpenSSL servers are expected to reset 
 *    connection.
 *
 * Returns:
 *  0 if all tests conforms to the expected OpenSSL behaviour
 *  -1 if unknown failure
 * exit codes from c_probe_utils.h are available
 * exit codes > 100 specifies the number of tests that did not conform to 
 *              the expected OpenSSL beahviour, 
 *              e.g. return code 103 -> 3 tests did not conform
 */

#include "c_probe_utils.h"

#define TEST_FAILED 1
#define TEST_OK 0

// Flag to print verbose information about SSL records being sent
int STACK_VERBOSE = 0;

// Test cases
int clienthello_payload_ff_ff(int sock);
int clienthello_msg_payload_00_00(int sock);
int clienthello_msg_payload_ff_ff(int sock);
int invalid_compression_method(int sock);
int invalid_cipher_suites(int sock);
int clientkeyex_payload_00_00(int sock);
// Add new test case here and update NR_TESTS and array below
// and add it to the initalisation in c_stack_probe

// Pointer to the test cases
const int NR_TESTS = 6;
int (*test[6]) (int sock);

// Probes the server for the feature
// See description at top of file for return codes used.
int c_stack_probe(const char* host, int port, int verbose)
{
  STACK_VERBOSE = verbose;
  if (STACK_VERBOSE) printf("probe_stack -- Entered probe.\n");
  
  int ret = -1; // return value
  int     err;

  // For the socket
  int     sock;
  struct sockaddr_in server_addr;
  
  // Initialise test vector
  test[0] = clienthello_payload_ff_ff;
  test[1] = clienthello_msg_payload_00_00;
  test[2] = clienthello_msg_payload_ff_ff;
  test[3] = invalid_compression_method;
  test[4] = clientkeyex_payload_00_00;
  test[5] = invalid_cipher_suites;
  
  
  // Get random bytes
  unsigned char* rand_bytes = (unsigned char*)malloc(sizeof(unsigned char)*28);
  RAND_bytes(rand_bytes, 28);
  
  // Get seconds since epoch
  time_t rawtime = time(NULL); // now
  
  unsigned char* epoch_time = (unsigned char*)malloc(sizeof(unsigned char)*4);
  //unsigned long n = rawtime;
  epoch_time[0] = (rawtime >> 24) & 0xFF;
  epoch_time[1] = (rawtime >> 16) & 0xFF;
  epoch_time[2] = (rawtime >> 8) & 0xFF;
  epoch_time[3] = rawtime & 0xFF;
  
  // Debug info
  if (STACK_VERBOSE) {
    printf("## INFO: rand bytes");
    print_hex(rand_bytes, 28);
    
    printf("\n## INFO: epoch time (%ju sec)", rawtime);
    print_hex(epoch_time, 4);
    printf("\n");
  }
  
  // Construct clienthello_data[343] as
  // [ clienthello_data[6], epoch_time[4], rand_bytes[28], clienthello_data2[305] ]
  memcpy(&clienthello_data[6], epoch_time, 4);
  memcpy(&clienthello_data[10], rand_bytes, 28);
  memcpy(&clienthello_data[38], clienthello_data2, clienthello_data2_size);
  
  /**********************************************/
  /* Client hello constructed and ready for use */
  /**********************************************/
  
  // Perform the tests
  if (STACK_VERBOSE) printf("probe_stack -- Starting tests...\n");
  
  // Iterate over test vector
  for (int i = 0; i < NR_TESTS; ++i) {
    printf("probe_stack -- Starting test  %d out of %d\n", i+1, NR_TESTS);
    
    // Set up a TCP socket
    err = setup_TCP_socket(&sock, &server_addr, host, port);
    if (err == -1) {
      printf("errno: %d\n", errno);
      printf("probe_stack -- ERROR: Connect of TCP socket failed.\n");
      return EXIT_CONNECT_FAILURE;
    }
    
    // Run the test
    ret += (*test[i]) (sock);
    
    // Shutdown connection in both directions
    // done with send_close_notify in test[i]
    
    printf("probe_stack -- Completed test %d out of %d\n", i+1, NR_TESTS);
  }
  if (STACK_VERBOSE) printf("probe_stack -- Tests done..\n");
  
  free(rand_bytes);
  free(epoch_time);
  
  // Conform to the return code specified at top of page
  if(ret > -1) ret += 100;
  return ++ret;
}

// Sets the record payload to 0xff 0xff in the clienthello message
// Expected response from an OpenSSL server is record_overflow
int clienthello_payload_ff_ff(int sock) {
  int ret = -100;
  
  // Store original values
  unsigned char* old_values = (unsigned char*)malloc(sizeof(unsigned char)*2);
  memcpy(old_values, &clienthello_header[3], 2);
  
  // Payload length set to ff ff
  clienthello_header[3] = 0xff;
  clienthello_header[4] = 0xff;
  
  if (STACK_VERBOSE) {
    printf("probe_stack::clienthello_payload_ff_ff -- sending clienthello: ");
    print_hex(clienthello_data, clienthello_data_size);
  }
  
  // Write client_hello
  write(sock, clienthello_header, clienthello_header_size);
  write(sock, clienthello_data, clienthello_data_size);
  
  if (STACK_VERBOSE) printf("probe_stack::clienthello_payload_ff_ff -- \
INFO: Waiting for reply.\n");
  
  
  // OpenSSL servers will respond with Alert::fatal::record_overflow
  struct SSL_record_info record;
  record = read_SSL_record(sock);
  if (record.data_size > 0 &&
      record.type == SSL_RECORD_TYPE__ALERT &&
      record.subsubtype == SSL_ALERT_RECORD_OVERFLOW) {
    // Server responded with alert
    if (STACK_VERBOSE) printf("probe_stack::clienthello_payload_ff_ff -- \
INFO: Server responded with expected alert: 0x%02x\n",
                              record.subsubtype);
    ret = TEST_OK;
  } else {
    // Server did not respond with expected alert
    if (STACK_VERBOSE) printf("probe_stack::clienthello_payload_ff_ff -- \
INFO: Server did not respond with expected alert\n");
    ret = TEST_FAILED;
  }
  
  if (STACK_VERBOSE || ret == 1) {
    printf("probe_stack::clienthello_payload_ff_ff -- \
expected alert 0x%02x, received following record:\n",
           SSL_ALERT_RECORD_OVERFLOW);
    print_SSL_record_info(record);
  }
  
  send_close_notify(sock, record);
  
  // Reset payload length
  memcpy(&clienthello_header[3], old_values, 2);

  free(old_values);
  return ret;
}

// Sets the message payload to 0x00 0x00 in the clienthello message
// Expected response from an OpenSSL server is alert::protocol_version
int clienthello_msg_payload_00_00(int sock) {
  int ret = -100;
  
  // Store original values
  unsigned char* old_values = (unsigned char*)malloc(sizeof(unsigned char)*3);
  memcpy(old_values, &clienthello_data[1], 3);
  
  // Set msg payload length
  clienthello_data[1] = 0x00;
  clienthello_data[2] = 0x00;
  clienthello_data[3] = 0x00;
  
  if (STACK_VERBOSE) {
    printf("probe_stack::clienthello_msg_payload_00_00 -- \
sending clienthello: ");
    print_hex(clienthello_data, clienthello_data_size);
  }

  // Write client_hello
  write(sock, clienthello_header, clienthello_header_size);
  write(sock, clienthello_data, clienthello_data_size);
  
  if (STACK_VERBOSE) printf("probe_stack::clienthello_msg_payload_00_00 -- \
INFO: Waiting for reply.\n");
  
  
  // OpenSSL servers will respond with
  // Alert::fatal::protocol_version || Alert::fatal::decode_error
  struct SSL_record_info record;
  record = read_SSL_record(sock);
  if (record.data_size > 0 &&
      record.type == SSL_RECORD_TYPE__ALERT &&
      (record.subsubtype == SSL_ALERT_PROTOCOL_VERSION ||
       record.subsubtype == SSL_ALERT_DECODE_ERROR) ) {
    // Server responded with alert
    if (STACK_VERBOSE) printf("probe_stack::clienthello_msg_payload_00_00 -- \
INFO: Server responded with expected alert: 0x%02x\n",
                              record.subsubtype);
    ret = TEST_OK;
  } else {
    // Server did not respond with expected alert
    printf("probe_stack::clienthello_msg_payload_00_00 -- \
INFO: Server did not respond with expected alert\n");
    ret = TEST_FAILED;
  }
  
  if (STACK_VERBOSE || ret == 1) {
    printf("probe_stack::clienthello_msg_payload_00_00 -- \
expected alert 0x%02x or 0x%02x, received following record:\n",
           SSL_ALERT_PROTOCOL_VERSION, SSL_ALERT_DECODE_ERROR);
    print_SSL_record_info(record);
  }
  
  send_close_notify(sock, record);
  
  // Reset payload length
  memcpy(&clienthello_data[1], old_values, 3);
  
  if (STACK_VERBOSE) printf("probe_stack::clienthello_msg_payload_00_00 -- \
INFO: Returning: %d.\n",
                            ret);
  
  free(old_values);
  return ret;
}

// Sets the message payload to 0xff 0xff in the clienthello message
// Expected response from an OpenSSL server is alert::illegal_parameter
int clienthello_msg_payload_ff_ff(int sock) {
  int ret = -100;
  
  // Store original values
  unsigned char* old_values = (unsigned char*)malloc(sizeof(unsigned char)*3);
  memcpy(old_values, &clienthello_data[1], 3);

  // Set msg payload length
  clienthello_data[1] = 0x00;
  clienthello_data[2] = 0xff;
  clienthello_data[3] = 0xff;
  
  if (STACK_VERBOSE) {
    printf("probe_stack::clienthello_msg_payload_ff_ff -- \
sending clienthello: ");
    print_hex(clienthello_data, clienthello_data_size);
  }
  
  // Write client_hello
  write(sock, clienthello_header, clienthello_header_size);
  write(sock, clienthello_data, clienthello_data_size);
  
  if (STACK_VERBOSE) printf("probe_stack::clienthello_msg_payload_ff_ff -- \
INFO: Waiting for reply.\n");
  
  
  // OpenSSL servers will respond with
  // Alert::fatal::illegal_parameter
  struct SSL_record_info record;
  record = read_SSL_record(sock);
  if (record.data_size > 0 &&
      record.type == SSL_RECORD_TYPE__ALERT &&
      (record.subsubtype == SSL_ALERT_ILLEGAL_PARAMETER) ) {
        // Server responded with alert
        if (STACK_VERBOSE) printf("probe_stack::clienthello_msg_payload_ff_ff -- \
INFO: Server responded with expected alert: 0x%02x\n",
                                  record.subsubtype);
        ret = TEST_OK;
      } else {
        // Server did not respond with expected alert
        printf("probe_stack::clienthello_msg_payload_ff_ff -- \
INFO: Server did not respond with expected alert\n");
        ret = TEST_FAILED;
      }
  
  if (STACK_VERBOSE || ret == 1) {
    printf("probe_stack::clienthello_msg_payload_ff_ff -- \
expected alert 0x%02x, received following record:\n",
           SSL_ALERT_ILLEGAL_PARAMETER);
    print_SSL_record_info(record);
  }
  
  send_close_notify(sock, record);
  
  // Reset payload length
  memcpy(&clienthello_data[1], old_values, 3);
  
  free(old_values);
  return ret;
}


// Sets the compression methods to unassigned values
int invalid_compression_method(int sock) {
  int ret = -100;
  
  // Store original values
  unsigned char* old_values = (unsigned char*)malloc(sizeof(unsigned char)*2);
  memcpy(old_values, &clienthello_data[224], 2);
  
  // Set compression support
  clienthello_data[224] = 0x64; // 0x64 is unassigned
  clienthello_data[225] = 0x65; // 0x65 is unassigned
  
  if (STACK_VERBOSE) {
    printf("probe_stack::invalid_compression_method -- \
sending clienthello: ");
    print_hex(clienthello_data, clienthello_data_size);
  }
  
  // Write client_hello
  write(sock, clienthello_header, clienthello_header_size);
  write(sock, clienthello_data, clienthello_data_size);
  
  if (STACK_VERBOSE) printf("probe_stack::invalid_compression_method -- \
INFO: Waiting for reply.\n");
  
  // OpenSSL servers will respond with
  // Alert::fatal::decode_error
  struct SSL_record_info record;
  record = read_SSL_record(sock);
  if (record.data_size > 0 &&
      record.type == SSL_RECORD_TYPE__ALERT &&
       record.subsubtype == SSL_ALERT_DECODE_ERROR) {
        // Server responded with alert
        if (STACK_VERBOSE) printf("probe_stack::invalid_compression_method -- \
INFO: Server responded with expected alert: 0x%02x\n",
                                  record.subsubtype);
        ret = TEST_OK;
      } else {
        // Server did not respond with expected alert
        printf("probe_stack::invalid_compression_method -- \
INFO: Server did not respond with expected alert\n");
        ret = TEST_FAILED;
      }
  
  if (STACK_VERBOSE || ret == 1) {
    printf("probe_stack::invalid_compression_method -- \
expected alert 0x%02x, received following record:\n",
           SSL_ALERT_DECODE_ERROR);
    print_SSL_record_info(record);
  }
  
  send_close_notify(sock, record);
  
  // Reset payload length
  memcpy(&clienthello_data[224], old_values, 2);

  free(old_values);
  return ret;

}

// Sets the length of the cipher list to 0 and modifies size parameters accordingly
// Expected response from an OpenSSL server is alert::decode_error
int invalid_cipher_suites(int sock) {
  int ret = -100;
  
  unsigned char mod_data[9] = {
    SSL_HANDSHAKE_TYPE__CLIENT_HELLO, // type
    0x00, 0x00, 0x9d, // size
    SSL_MAJOR_VERSION__SSL3_TLS, // client major version
    SSL_MINOR_VERSION__TLS1_2, // client minor version
                               // timestamp and randbytes will go in between here
    0x00, // session id 1-32 bytes
    0xfe, 0xee // Supported ciphers, 0xfe 0xee is unasigned
  };
  
  unsigned char* clienthello_data_mod = (unsigned char*)malloc(sizeof(unsigned char)*161);
  memset(clienthello_data_mod, 0, 161);
  
  // Construct clienthello
  memcpy(&clienthello_data_mod[0], &mod_data[0], 6);
  memcpy(&clienthello_data_mod[6], &clienthello_data[6], 32); // get epoch_time and rand bytes from global var
  memcpy(&clienthello_data_mod[38], &mod_data[6], 3);
  memcpy(&clienthello_data_mod[41], &clienthello_data2[185], 120);
  
  unsigned char clienthello_mod_header[5] = {SSL_RECORD_TYPE__HANDSHAKE,
    SSL_MAJOR_VERSION__SSL3_TLS,
    SSL_MINOR_VERSION__TLS1_0,
    0x00, 0xa1}; // size
  
  if (STACK_VERBOSE) {
    printf("probe_stack::invalid_cipher_suites --  \
sending clienthello: ");
    print_hex(clienthello_data_mod, 161);
  }
  
  // Write client_hello
  write(sock, clienthello_mod_header, 5);
  write(sock, clienthello_data_mod, 161);
  
  printf("probe_stack::invalid_cipher_suites -- \
INFO: Expecting connection reset.\n");
  
  // OpenSSL servers will reset the connection
  struct SSL_record_info record;
  record = read_SSL_record(sock);
  if (record.data_size == 0 ) {
    // Server responded as expected
    if (STACK_VERBOSE) printf("probe_stack::invalid_cipher_suites -- \
INFO: Server responded as expected\n");
    ret = TEST_OK;
  } else {
    // Server did not respond  as expected
    printf("probe_stack::invalid_cipher_suites -- \
INFO: Server responded with alert, not expected\n");
    ret = TEST_FAILED;
  }
  
  if (STACK_VERBOSE || ret == 1) {
    printf("probe_stack::invalid_cipher_suites -- \
expected connection reset, received following record:\n");
    print_SSL_record_info(record);
  }
  
  send_close_notify(sock, record);
  
  free(clienthello_data_mod);
  return ret;
}

// Sets the record payload to 0xff 0xff in the clientkeyexchange message
// Expected response from an OpenSSL server is alert PRTOCOL_VERSION
int clientkeyex_payload_00_00(int sock) {
  int ret = -100;
  
  if (STACK_VERBOSE) {
    printf("probe_stack::clientkeyex_payload_00_00 -- sending valid clienthello: ");
  }
  
  // Write client_hello
  write(sock, clienthello_header, clienthello_header_size);
  write(sock, clienthello_data, clienthello_data_size);
  
  if (STACK_VERBOSE) printf("probe_stack::clientkeyex_payload_00_00 -- \
INFO: Waiting for reply.\n");
  
  // Expect serverhello
  unsigned char server_minor_version = SSL_MINOR_VERSION__TLS1_0;
  int server_hello_received = 0;
  struct SSL_record_info record;
  do {
    record = read_SSL_record(sock);
    if (record.data_size <= 0)
      return 1;
    if (record.data_size > 0 && record.type == SSL_RECORD_TYPE__HANDSHAKE) {
      if (record.subtype == SSL_HANDSHAKE_TYPE__SERVER_HELLO) {
        server_hello_received = 1;
        // Use the supported protocol in subsequent communication
        server_minor_version = record.minor_version;
        if (record.data_size > 800) break; // Handle several records sent in one packet
      }
    }
  }while (record.data_size > 0 &&
             record.type != SSL_RECORD_TYPE__ALERT &&
             (record.type == SSL_RECORD_TYPE__HANDSHAKE &&
              record.subtype != SSL_HANDSHAKE_TYPE__SERVER_DONE) );
  
  if (!server_hello_received) {
    if (STACK_VERBOSE) {
      printf("probe_stack::clientkeyex_payload_00_00 -- \
ERROR: Server did not respond with expected server_hello_done\n");
    }
    return TEST_FAILED;
  }
  
  unsigned char* keyex_header = (unsigned char*)malloc(sizeof(unsigned char)*5);
  memset(keyex_header, 0, 5);
  // Payload length set to 00 00 from memset
  keyex_header[0] = SSL_RECORD_TYPE__HANDSHAKE;
  keyex_header[1] = SSL_MAJOR_VERSION__SSL3_TLS;
  keyex_header[2] = server_minor_version;
  
  unsigned char* keyex_data = (unsigned char*)malloc(sizeof(unsigned char)*70);
  memset(keyex_data, 0, 70);
  keyex_data[0] = SSL_HANDSHAKE_TYPE__CLIENT_KEY_EXCHANGE;
  keyex_data[3] = 0x42;
  keyex_data[4] = 0x41;
  RAND_bytes(&keyex_data[5], 65); // In reality the key exchange payload
  
  if (STACK_VERBOSE) {
    printf("probe_stack::clientkeyex_payload_00_00 -- sending client key exchange: ");
    print_hex(keyex_header, 5);
    print_hex(keyex_data, 70);
  }
  
  // Write client_keyexchange
  write(sock, keyex_header, 5);
  write(sock, keyex_data, 70);
  
  // OpenSSL servers will respond with Alert::fatal::record_overflow
  
  record = read_SSL_record(sock);
  if (record.data_size > 0 &&
      record.type == SSL_RECORD_TYPE__ALERT &&
      record.subsubtype == SSL_ALERT_PROTOCOL_VERSION) {
    // Server responded with alert
    if (STACK_VERBOSE) printf("probe_stack::clientkeyex_payload_00_00 -- \
INFO: Server responded with expected alert: 0x%02x\n",
                              record.subsubtype);
    ret = 0;
  } else {
    // Server did not respond with expected alert
    if (STACK_VERBOSE) printf("probe_stack::clientkeyex_payload_00_00 -- \
INFO: Server did not respond with expected alert\n");
    ret = 1;
  }
  
  if (STACK_VERBOSE || ret == 1) {
    printf("probe_stack::clientkeyex_payload_00_00 -- \
expected alert 0x%02x, received following record:\n",
           SSL_ALERT_PROTOCOL_VERSION);
    print_SSL_record_info(record);
  }
  
  send_close_notify(sock, record);
  
  free(keyex_header);
  free(keyex_data);
  return ret;
}

//
//  probe_alert_tautology.cpp
//  aprover
//
//  Created by Martin Andersson on 24/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Probes for the commit found here:
 * https://github.com/openssl/openssl/commit/11414f17d93ec04d2d056d5f3a87e964a7412431
 * It initiates a handshake and then sends a close notify with an invalid
 * version number set in the header. In affected versions this trigger sending
 * an alert back with the invalid version and SSL_ALERT_PROTOCOL_VERSION.
 * Newer versions do not send an alert back, they only close down the connection.
 * Loads: probe_alert_tautology.c
 *
 * Affects version: 0.9.8:0.9.8zg, 1.0.0:1.0.0s, 1.0.1:1.0.1m, 1.0.2:1.0.2a
 * Note that versions after 0.9.8zg and 1.0.0s may or may not be affected by
 * this depending on if the commit is implemented in those branches as well.
 */

#include "probe.h"
#include <stdlib.h>

#include <stdio.h>
#include <stdlib.h>

using namespace std;

class C_Probe_Changelog_Alert : public Probe {
public:
  virtual int run(Server&, bool verbose);
private:
};

typedef int (*FUNC_PTR)(const char*, int, int);

int C_Probe_Changelog_Alert::run(Server& s, bool verbose) {
  int ret = 0;
  this->set_status(FAILED);
  
  void *handle;
  int (*probe)(const char*, int, int);
  char *error;
  
  handle = dlopen("lib/probe_alert_tautology_c.so", RTLD_LAZY);
  if (!handle) {
    fputs (dlerror(), stderr);
    return 1;
  }
  
  probe = (FUNC_PTR)dlsym(handle, "c_probe_alert_tautology");
  if ((error = dlerror()) != NULL)  {
    fputs(error, stderr);
    return 1;
  }
  
  int c_ret = probe(s.get_host().c_str(), s.get_port(), verbose);
  s.add_info("probe_alert_tautology -- ");
  
  if (c_ret == 0) {
    cout << "Not affected, no alert received" << endl;
    s.add_info("Not affected, no alert received\n");
    this->set_status(NOT_AFFECTED);
  }
  else if  (c_ret == 1) {
    cout << "Affected, alert received" << endl;
    s.add_info("Affected, alert received\n");
    this->set_status(AFFECTED);
  }
  else if (c_ret == 6) {
    // Handshake failure
    ret = 1;
    s.add_info("Handshake missing or invalid, aborted.\n");
    throw probe_error("Handshake missing or invalid, aborted.\n");
  }
  else if (c_ret == 7) {
    // SSL record failure
    ret = 1;
    s.add_info("SSL record declared negative data size.\n");
    throw probe_error("SSL record declared negative data size.\n");
  }
  else if (c_ret == 8) {
    // Connect failure
    ret = 1;
    s.add_info("Set up or connect of TCP socket failed.\n");
    throw probe_error("Set up or connect of TCP socket failed.\n");
  }
  else if (c_ret == 9) {
    // Lookup failure
    ret = 1;
    s.add_info("Could not look up host.\n");
    throw probe_error("Could not look up host.\n");
  }
  else if (c_ret == 10) {
    // Unknown failure
    ret = 1;
    s.add_info("Unknown failure occured in probe_alert_tautology.c.\n");
    throw probe_error("Unknown failure occured in probe_alert_tautology.c.\n");
  }
  else {
    ret = 1;
    s.add_info("Unknown return code from library.\n");
    throw probe_error("Unknown return code from library.\n");
  }
  
  return ret;
}

// the class factories
extern "C" Probe* create() {
  return new C_Probe_Changelog_Alert;
}

extern "C" void destroy(Probe* p) {
  delete p;
}

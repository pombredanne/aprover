//  main_plugin.cpp
//  aprover_plugin
//
//  Created by Martin Andersson on 22/07/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Main functions for the Chrome plugin.
 * Handles messgaing between the C/C++ code and the Javasript running
 * in the browser.
 * Based on the example provided in:  
 * <nacl_sdk_root>/pepper_<version>/examples/tutorial/dlopen
 *
 * Author: Martin Andersson 2015-07-22
 * Original headers are kept below.
 */

// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.


#include <vector>
#include <string>
#include <iostream>
#include <dirent.h>
#include <time.h>

#include "classes.h"
#include "version.h"
#include "database.h"
#include "feature.h"
#include "probe.h"
#include "server.h"
#include "c_probe_utils.h"

// Native client API
#include <ppapi/cpp/completion_callback.h>
#include <ppapi/cpp/instance.h>
#include <ppapi/cpp/module.h>
#include <ppapi/cpp/var.h>
#include "nacl_io/nacl_io.h"
#include <sys/mount.h>

#if defined(NACL_SDK_DEBUG)
#define CONFIG_NAME "Debug"
#else
#define CONFIG_NAME "Release"
#endif

#if defined __arm__
#define NACL_ARCH "arm"
#elif defined __i686__
#define NACL_ARCH "x86_32"
#elif defined __x86_64__
#define NACL_ARCH "x86_64"
#else
#error "Unknown arch"
#endif

char* JSON_DIR;

using namespace std;

double get_wall_time(){
  struct timeval time;
  if (gettimeofday(&time,NULL)){
    //  Handle error
    return 0;
  }
  return (double)time.tv_sec + (double)time.tv_usec * .000001;
}


class PluginInstance : public pp::Instance {
 public:
  explicit PluginInstance(PP_Instance instance)
      : pp::Instance(instance) {}

  virtual ~PluginInstance() {}

  // Helper function to post a message back to the JS and stdout functions.
  void logmsg(const char* pStr) {
    PostMessage(pp::Var(std::string("log:") + pStr));
    printf("log:%s\n",pStr);
  }

  void infomsg(const char* pStr) {
    PostMessage(pp::Var(std::string("info:") + pStr));
    printf("info:%s\n",pStr);
  }

  void errormsg(const char* pStr) {
    PostMessage(pp::Var(std::string("error:") + pStr));
    printf("error:%s\n",pStr);
  }

  // Initialize the module, staring a worker thread to load the shared object.
  virtual bool Init(uint32_t argc, const char* argn[], const char* argv[]) {
    nacl_io_init_ppapi(pp_instance(),
                       pp::Module::Get()->get_browser_interface());
    // Mount a HTTP mount at /http. All reads from /http/* will read from the
    // server.
    mount("", "/http", "httpfs", 0, "");
    mount("incl/", "", "httpfs", 0, "");

    db_open_ = false;
    logmsg("Spawning thread to open database file...");
    // Needed since files and sockets can not be accessesd in main thread
    if (pthread_create(&db_tid_, NULL, LoadDBOnWorker, this)) {
      errormsg("ERROR; pthread_create(... LoadDBOnWorker ...) failed.");
      return false;
    }
    return true;
  }

  void LoadDB() {
    try {
      const char db_path[] = "/http/glibc/" CONFIG_NAME "/aprover_web.db";
      string web_dir = "not_used_in_plugin";
      JSON_DIR = (char*)web_dir.c_str();
      Database::open(db_path);
      Database::set_max_destructive(2);
    } catch (exception& e) {
      errormsg(e.what());
    }
    logmsg("Database file opened succesfully");
    db_open_ = true;
    infomsg("send_url");
  }

  void list() {
    logmsg("list recieved...");
    stringstream ss;
    Database::list_all_features(ss);
    logmsg(ss.str().c_str()); 
  }

  // Returns true if the server behaves as an OpenSSL server
  bool stack() {
    Feature f = Database::search_name("stack test", false);
    f.probe(s_, true);
    return (s_.get_probe_results()[0].status == NOT_AFFECTED);
  }

  // Start the fingerprinting
  void start() {
    try {
      double begin, end;
      begin = get_wall_time();
      s_.set_host(url_);
      s_.set_port(port_);
      infomsg("stack");
      if (!stack()) {
        if (s_.get_probe_results()[0].status == FAILED) {
          errormsg("The plugin does not appear to have the correct permissions to use the socket API");
          infomsg("socketFail");
        }
        else {
          infomsg("notOpenSSL");
          logmsg(format_string_for_output(s_.get_info()).c_str());
        }
      }
      else {
        infomsg("running");
        s_.fingerprint(true, true); // enable verbose output to stdout, signal that we're in NaCl
        stringstream ss;
        s_.print_web(ss);
        logmsg(ss.str().c_str());
        infomsg(("version:" + serialise_versions(s_.get_most_likely_version())).c_str());
        end = get_wall_time();
        char buf[64];
        sprintf(buf, "Elapsed time: %lf seconds", end-begin);
        logmsg(buf);

      }
    } catch (exception& e) {
      cerr << "error:" << e.what() << endl;
      errormsg(e.what());
    }
  }

  // Called by the browser to handle the postMessage() call in Javascript.
  virtual void HandleMessage(const pp::Var& var_message) {
    try {
      if (!var_message.is_string()) {
        logmsg("Message is not a string.");
        return;
      }

      string message = var_message.AsString();
      if (message.find("reverse:") == 0) {
        string str = message.substr(strlen("reverse:"));
        if (str == "list") {
          list();
        }
      } else if (message.find("url:") == 0) {
        string url_str = message.substr(strlen("url:"));
          logmsg("url recieved in cpp: ");
          logmsg(url_str.c_str());
          url_ = url_str;
          if (!db_open_) {
            errormsg("The database is not open.");
            return;
          }
          
          logmsg("Spawning thread to fingerprint...");
          // Needed since files and sockets can not be accessesd in main thread
          if (pthread_create(&run_tid_, NULL, startOnWorker, this)) {
            errormsg("ERROR; pthread_create(... startOnWorker ...) failed.");
            return;
          }
      } else {
        string errormsg = "Unexpected message: ";
        errormsg += message;
        logmsg(errormsg.c_str());
      }

    } catch (exception& e) {
      cerr << "error:" << e.what() << endl;
      errormsg(e.what());
    }
  }
  
  static void* LoadDBOnWorker(void* pInst) {
    PluginInstance* inst = static_cast<PluginInstance*>(pInst);
    inst->LoadDB();
    return NULL;
  }

  static void* startOnWorker(void* pInst) {
    PluginInstance* inst = static_cast<PluginInstance*>(pInst);
    inst->start();
    return NULL;
  }


  private:  
    pthread_t db_tid_;
    pthread_t run_tid_;
    Server s_;
    string url_;
    static const int port_  = 443;
    bool db_open_;
};

class PluginModule : public pp::Module {
 public:
  PluginModule() : pp::Module() {}
  virtual ~PluginModule() {}

  // Create and return a PluginInstance object.
  virtual pp::Instance* CreateInstance(PP_Instance instance) {
    return new PluginInstance(instance);
  }
};

namespace pp {
Module* CreateModule() { return new PluginModule(); }
}  // namespace pp


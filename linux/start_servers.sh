#!/bin/bash
# This file should be placed in openssl/src
# Installed versions should be found in openssl/version
# with the binary at openssl/version/bin/openssl
# Keys and certificates are located in openssl/key_cert
#
# If no arguments are given, this script will start test servers
# for all found versions of OpenSSL starting at the default port 4433
# after the user confirmation has been given.
# The test servers are launched in a new xterm shells.
# 
# Options are:
# -b : Enables debug output. Equivalent to -o "-msg -debug".
# -d : Run server using DTLS
# -f : Do not ask for user confirmation before starting servers.
# -o : Additional options to pass onto the openssl s_server.
#      Should be given as a qouted string.
# -p : Specify starting port to use. Default is 4433. 
#      Needs to be given before the -v flag.
# -r : Specify a regex to use for matching versions to start.
# -s : Run with -cipher SUITEB128 and corresponding cert and key.
# -v : Specify versions to start. Several -v can be used. Should
#      be given last if several options are used.


set -e # Exit on first error

# BASE_DIR is openssl, i.e. one level up
BASE_DIR=$(cd `dirname .` && pwd)
BASE_DIR=$(readlink -f ${BASE_DIR}/..)

# Print version based on file name
print_version() {
 path=$1
 version=${path%/bin/openssl}
 version=${version#${BASE_DIR}/}
 echo $version
}

# Start an OpenSSL server
start_server() {
  file=$1
  if ! [ -f $file ]; then
    echo "OpenSSL not installed at: $file"
    exit
  fi
  echo -e "Starting ${DTLS_STR} server $(print_version $file) at port:  $PORT"
  xterm -e ${BASE_DIR}/src/run_server.sh $file $PORT $BASE_DIR $DTLS_FLAG $SUITE_B_FLAG "$OPTIONS" &
  PORT=$((PORT+1))
}

# Parameter flags
DTLS_FLAG=false
DTLS_STR=""
VERSION_FLAG=false
OPTIONS=""
REGEX=""
PORT=4433
DEBUG_FLAG=false
SUITE_B_FLAG=false

while getopts ":o:r:v:p:bds" opt; do
  case $opt in
    b)
      DEBUG_FLAG=true
      ;;
    d)
      DTLS_FLAG=true
      DTLS_STR="DTLS"
      ;;
    o)
      OPTIONS=$OPTARG
      ;;
    p)
      PORT=$OPTARG
      ;;
    r)
      REGEX=$OPTARG
      ;;
    s)
      SUITE_B_FLAG=true
      ;;
    v)
      VERSION_FLAG=true
      start_server $BASE_DIR/${OPTARG,,}/bin/openssl	
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

# Don't do look-up if version specified
if $VERSION_FLAG ; then
  exit
fi

# Add debug options if debug flag set
if $DEBUG_FLAG ; then
  OPTIONS="$OPTIONS -msg -debug"
fi

index=-1
echo "Select range of servers that will be started:"
find ${BASE_DIR} -maxdepth 3 -wholename "*${REGEX}/bin/openssl" -type f | sort | while read filename 
do
  index=$((index+1))
  printf "%2d: " "$index"
  print_version "$filename"
done

lower=$((index))
upper=$((index))
echo "-------------------"
printf "Select lower end of range as an integer: "
read input
if [ $input -eq $input 2>/dev/null ]; then
  # input is an integer
  lower=$((input))
else
  echo "$input is not an integer"
  exit
fi


printf "Select upper end of range as an integer: "
read input
if [ $input -eq $input 2>/dev/null ]; then
  # input is an integer
  upper=$((input))
else
  echo "$input is not an integer"
  exit
fi

index=-1
find ${BASE_DIR} -maxdepth 3 -wholename "*${REGEX}/bin/openssl" -type f | sort | while read file 
do
  index=$((index+1))
  if [[ $index -ge $lower ]] && [[ $index -le $upper ]]; then
   start_server "$file"
  fi
done


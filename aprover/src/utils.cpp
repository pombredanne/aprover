//
//  utils.cpp
//  aprover
//
//  Created by Martin Andersson on 24/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Utility functions used for output.
 */

#include "utils.h"

using namespace std;

// Replace bad filename characters and whitespace with given char, defaults to '_'
// \ / < > | " : ? *
string replace_invalid_file_characters(string in, const char c) {
  std::replace( in.begin(), in.end(), '\\', c);
  std::replace( in.begin(), in.end(), '/', c);
  std::replace( in.begin(), in.end(), '<', c);
  std::replace( in.begin(), in.end(), '>', c);
  std::replace( in.begin(), in.end(), '|', c);
  std::replace( in.begin(), in.end(), '"', c);
  std::replace( in.begin(), in.end(), ':', c);
  std::replace( in.begin(), in.end(), '?', c);
  std::replace( in.begin(), in.end(), '*', c);
  std::replace( in.begin(), in.end(), ' ', c);
  return in;
}

// Formats a string to fit within width and with a possible
// prefix leading to an indent
string format_string_for_output(const string s, const string prefix, int width, unsigned short indent) {
stringstream ss;
    // Generate indent
  if (prefix.size() > 0)
    indent = (unsigned short)prefix.size();
  string indent_str;
  for (unsigned short i = 0; i < indent; ++i)
    indent_str += " ";
  
  // Make same width independent of prefix or indent
  width -= indent;
  if (width < 40) {
    cout << "format_string_for_output -- Given width is smaller than 40. Using 40 instead." << endl;
    width = 40;
  }
  
  unsigned short row = 0;
  size_t full_size = s.size();
  size_t pos = 0;
  size_t newline = string::npos;
  int ws_width, extra_width; // width to best whitespace
  
  ss << prefix;
  while (full_size > pos) {
    ws_width = width;
    if (pos+(size_t)ws_width < full_size) {
      while (!isspace(s[pos+(size_t)ws_width-1]))
        --ws_width;
      if (ws_width <= 0) // if whitespace not found, cut at full width
        ws_width = width;
    }
    // scan for newline
    newline = string::npos;
    newline = (s.substr(pos, (size_t)ws_width)).find("\n");
    extra_width = (newline != string::npos);
    if (extra_width) ws_width = (int)newline;
    
    ss << ((row == 0) ? "": indent_str) << s.substr(pos, (size_t)ws_width) << endl;
    pos += (size_t)(ws_width+extra_width);
    ++row;
  }

  return ss.str();
}

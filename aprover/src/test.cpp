//
//  test.cpp
//  aprover
//
//  Created by Martin Andersson on 04/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Basic test functions used during development to test
 * elementary functionality of data structures.
 */


#include "test.h"
using namespace std;

// Test the enum and string representation of OpenSSL versions
void test_version_h(ostream& stream) {
  // Enum and vector representation
  if ("0.9.8" != OPENSSL_VERSION_name[int(OPENSSL_VERSION::EIGHT)])
    throw format_error("version.h test failed for 0.9.8");
  
  if ("0.9.8f" != OPENSSL_VERSION_name[int(OPENSSL_VERSION::EIGHT_F)])
    throw format_error("version.h test failed for 0.9.8f");
  
  if ("0.9.8zy" != OPENSSL_VERSION_name[int(OPENSSL_VERSION::EIGHT_ZY)])
    throw format_error("version.h test failed for 0.9.8zy");
  
  if ("1.0.0" != OPENSSL_VERSION_name[int(OPENSSL_VERSION::ZERO)])
    throw format_error("version.h test failed for 1.0.0");
  
  if ("1.0.0a" != OPENSSL_VERSION_name[int(OPENSSL_VERSION::ZERO_A)])
    throw format_error("version.h test failed for 1.0.0a");
  
  if ("1.0.1x" != OPENSSL_VERSION_name[int(OPENSSL_VERSION::ONE_X)])
    throw format_error("version.h test failed for 1.0.1x");
  
  if ("1.0.2k" != OPENSSL_VERSION_name[int(OPENSSL_VERSION::TWO_K)])
    throw format_error("version.h test failed for 1.0.2k");
  
  if ("1.0.2zy" != OPENSSL_VERSION_name[int(OPENSSL_VERSION::TWO_ZY)])
    throw format_error("version.h test failed for 1.0.2zy");
  
  
  // Test version_str2enum and version_enum2str
  if ("0.9.8" != version_enum2str(version_str2enum("0.9.8")))
    throw format_error("version.h test failed for 1.0.2zg");
  
  if ("1.0.2zg" != version_enum2str(version_str2enum("1.0.2zg")))
    throw format_error("version.h test failed for 1.0.2zg");
  
  if ("unknown" != version_enum2str(version_str2enum("1.9.9aa")))
    throw format_error("version.h test failed for 1.0.2zz");
  
  stream << "Test of version.h succeded" << endl;
}


// Test the serialise functions used to convert
// vector with OPENSSL_VERSION enums <=> string
void test_serialise(ostream& stream) {
  vector<OPENSSL_VERSION> v, v2;
  for (int i=3; i<=5; i++) v.push_back(static_cast<OPENSSL_VERSION>(i));
  
  v.insert(v.begin(), OPENSSL_VERSION::EIGHT);
  v.push_back(OPENSSL_VERSION::EIGHT_G);
  
  string s = "0.9.8, 0.9.8b:0.9.8d, 0.9.8g";
  v2 = unserialise_versions(s);
  if (v != v2)
    throw feature_error("Test 1 of serialise failed!\n");
  
  string s2 = serialise_versions(v);
  if (s != s2)
    throw feature_error("Test 2 of serialise failed!\n");
  
  if (v != unserialise_versions(serialise_versions(v)) )
    throw feature_error("Test 3 of serialise failed!\n");
  
  stream << "Test of serialise succeded" << endl;
}

// Test the database
void test_database(ostream& stream) {
  const char* db_file = "test.db";
  Database::open(db_file);
  Database::update_from_json();
  
  vector<OPENSSL_VERSION> v;
  v.push_back(OPENSSL_VERSION::EIGHT_B);
  v.push_back(OPENSSL_VERSION::EIGHT_G);
  v.push_back(OPENSSL_VERSION::ZERO);
  v.push_back(OPENSSL_VERSION::ONE_ZY);
  v.push_back(OPENSSL_VERSION::TWO_A);
  
  Feature f("test feature", "only used to test the db interface", v, 3, 1, "test.so", 3, "no dependency");
  Database::feature_update(f);
  
  // TLS 1.2 support
  vector<OPENSSL_VERSION> v2;
  for (int i = int(OPENSSL_VERSION::ONE);
       i <= int(OPENSSL_VERSION::ONE_M);
       ++i)
    v2.push_back(static_cast<OPENSSL_VERSION>(i));
  for (int i = int(OPENSSL_VERSION::TWO);
       i <= int(OPENSSL_VERSION::TWO_A);
       ++i)
    v2.push_back(static_cast<OPENSSL_VERSION>(i));
  Feature f2("TLS 1.2 support", "Support", v2, 1, 1, "TLS1_2.so", 4, "dep");
  Database::feature_update(f2);
  
  // DTLS 1.2 support
  vector<OPENSSL_VERSION> v3;
  for (int i = int(OPENSSL_VERSION::TWO);
       i <= int(OPENSSL_VERSION::TWO_A);
       ++i)
    v3.push_back(static_cast<OPENSSL_VERSION>(i));
  Feature f3("DTLS 1.2 support", "Support", v3, 1, 1, "DTLS1_2.so", 4 ,"");
  Database::feature_update(f3);
  
  // Test search_name
  Feature f9 = Database::search_name("test feature");
  f9.print(stream);
  
  Database::list_all_features(stream);
  // Test feature_remove
  stream << "Removing '" << f9.get_name() << "'" <<  endl;
  Database::feature_remove(f9);
  Database::list_all_features(stream);
  try {
    Feature f8 = Database::search_name("test feature");
    // This should not print
    stream << "This should not happen because this feature is removed" << endl;
    f8.print(stream);
  }
  catch (exception& e) {
    // We want to end up here
    stream << e.what();
  }
  Database::feature_update(f);
  
  Database::update_from_json();
  Feature f6 = Database::search_name("TLS 1.2 support");
  f6.print(stream);
  
  vector<Feature> f_vec, res;
  vector<OPENSSL_VERSION> ver_vec;
  ver_vec.push_back(OPENSSL_VERSION::TWO);
  ver_vec.push_back(OPENSSL_VERSION::ONE);
  res = Database::search_affected_excluding_probed(ver_vec, f_vec);
  
  
  
  
  Database::close();
  
  stream << "Test of database succeded" << endl;
}


// Test JSON parsing
void test_json(ostream& stream) {
  Feature f("json/test.json");
  f.print(stream);
  
  stream << "Test of json succeeded" << endl;
}


// Test dynamic libraries loaded on runtime
void test_dylib(Server& s, ostream & stream) {
  Database::open("aprover.db");
  Database::update_from_json();
  Feature f = Database::search_name("test feature");
  Database::close();
  f.probe(s, true);
  s.print();
  
  stream << "Test of dylib succeded" << endl;
}

// Test dynamic C libraries loaded on runtime
void test_c_dylib(Server& s, ostream & stream) {
  Database::open("aprover.db");
  Database::update_from_json();
  Feature f = Database::search_name("TLS 1.2 support");
  Database::close();
  f.probe(s, true);
  s.print_verbose();
  
  stream << "Test of C dylib succeded" << endl;
}

// Test given probe
void test_probe(Server& s, const std::string& name, ostream& stream) {
  Database::open("aprover.db");
  Database::update_from_json();
  Feature f = Database::search_name(name);
  Database::close();
  f.probe(s, true);
  s.print_verbose();
  
  stream << "Test of: " << name << " succeded" << endl;
}

// Use test server
void all_tests(ostream& stream) {
  Server s(TEST_HOST, TEST_PORT);
  all_tests(s, stream);
}

// Perform all tests
void all_tests(Server& s, ostream& stream) {
  try {
    test_version_h(stream);
    test_serialise(stream);
    test_database(stream);
    test_json(stream);
    test_dylib(s, stream);
    test_c_dylib(s, stream);
  }
  catch (exception& e) {
    cerr << "****************" << endl;
    cerr << e.what();
    cerr << "Tests failed" << endl;
    exit(1);
  }
  
  cout << "****************" << endl;
  cout << "All tests succeded!" << endl;
}


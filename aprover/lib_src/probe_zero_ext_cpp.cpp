//
//  probe_zero_ext.c
//
//
//  Created by Martin Andersson on 03/07/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Probes for the commit found here:
 * https://github.com/openssl/openssl/commit/fe64245aa1b1f5519ddfe11e3c9d7ad49ae4de95
 *
 * Unaffected versions allow for a zero length
 * extension block in the ClientHello message. 
 * Affected versions will instead respond with a DECODE_ERROR alert.
 *
 * Affects version: 1.0.1n and 1.0.2b
 */


#include "probe.h"
#include <stdlib.h>

#include <stdio.h>
#include <stdlib.h>

using namespace std;

class C_Probe_Zero_Ext : public Probe {
public:
  virtual int run(Server&, bool verbose);
private:
};

typedef int (*FUNC_PTR)(const char*, int, int);

int C_Probe_Zero_Ext::run(Server& s, bool verbose) {
  int ret = 0;
  this->set_status(FAILED);
  
  void *handle;
  int (*probe)(const char*, int, int);
  char *error;
  
  handle = dlopen("lib/probe_zero_ext_c.so", RTLD_LAZY);
  if (!handle) {
    fputs (dlerror(), stderr);
    return 1;
  }
  
  probe = (FUNC_PTR)dlsym(handle, "c_zero_ext_probe");
  if ((error = dlerror()) != NULL)  {
    fputs(error, stderr);
    return 1;
  }
  
  int c_ret = probe(s.get_host().c_str(), s.get_port(), verbose);
  s.add_info("probe_zero_ext -- ");
  
  if (c_ret == 0) {
    cout << "Server responded with ServerHello, i.e. not affected." << endl;
    s.add_info("Server responded with ServerHello, i.e. not affected.\n");
    this->set_status(NOT_AFFECTED);
  }
  else if  (c_ret == 1) {
    cout << "Server responded with alert DECODE_ERROR, i.e. affected.\n" << endl;
    s.add_info("Server responded with alert DECODE_ERROR, i.e. affected.\n");
    this->set_status(AFFECTED);
  }
  else if (c_ret == 6) {
    // Handshake failure
    ret = 1;
    s.add_info("Handshake missing or invalid, aborted.\n");
    throw probe_error("Handshake missing or invalid, aborted.\n");
  }
  else if (c_ret == 7) {
    // SSL record failure
    ret = 1;
    s.add_info("SSL record declared negative data size.\n");
    throw probe_error("SSL record declared negative data size.\n");
  }
  else if (c_ret == 8) {
    // Connect failure
    ret = 1;
    s.add_info("Set up or connect of TCP socket failed.\n");
    throw probe_error("Set up or connect of TCP socket failed.\n");
  }
  else if (c_ret == 9) {
    // Lookup failure
    ret = 1;
    s.add_info("Could not look up host.\n");
    throw probe_error("Could not look up host.\n");
  }
  else if (c_ret == 10) {
    // Unknown failure
    ret = 1;
    s.add_info("Unknown failure occured in probe_zero_ext.c.\n");
    throw probe_error("Unknown failure occured in probe_zero_ext.c.\n");
  }
  else {
    ret = 1;
    s.add_info("Unknown return code from library.\n");
    throw probe_error("Unknown return code from library.\n");
  }
  
  return ret;
}

// the class factories
extern "C" Probe* create() {
  return new C_Probe_Zero_Ext;
}

extern "C" void destroy(Probe* p) {
  delete p;
}

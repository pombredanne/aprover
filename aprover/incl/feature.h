//
//  feature.h
//  aprover
//
//  Created by Martin Andersson on 01/06/15.
//  Copyright (c) 2015 Martin Andersson. All rights reserved.
//

/*
 * Feature is the class used to hold objects specifying a feature to probe for.
 * The features are initalised from the entries in the database which in turn can be 
 * defined from JSON files.
 * Uses the publically available rapidjson parser from here:
 * https://github.com/miloyip/rapidjson
 */

#ifndef __aprover__feature__
#define __aprover__feature__

#include "myexceptions.h"

#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cerrno>
#include <dirent.h>
#include <dlfcn.h>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "classes.h"
#include "version.h"
#include "probe.h"
#include "server.h"
#include "utils.h"


// Directory where the JSON files are located
extern char* JSON_DIR;

/*
 * feature_error: error associated with the Feature class
 * takes explanantion as argument
 */
class feature_error : public std::logic_error
{
public:
  explicit feature_error(const std::string& what_arg) throw() // c++0x // c++11 noexcept
  : logic_error(what_arg) {}
  
  explicit feature_error(const char* what_arg) throw() // c++0x // c++11 noexcept
  : logic_error(what_arg) {}
};


/**
 * Feature: Class to represent the features to probe for
 */
class Feature
{
public:
  Feature() = default;
  Feature(const std::string& name,
          const std::string& description,
          const std::vector<OPENSSL_VERSION>& affected_versions,
          const int& access_complexity,
          const int& destructive,
          const std::string& lib,
          const int& conf,
          const std::string& dep);
  Feature(const char* jsonfile);
  Feature(const Feature&) = default;           // copy constr
  //Feature(Feature&&) = default;                // move constr // leave out for c++0x
  
  //~Feature() noexcept = default; // leave out for c++0x
  
  Feature& operator=(const Feature&) = default;  // copy assignment
  //Feature& operator=(Feature&&) = default;       // move assignment // leave out for c++0x
  
  void print(std::ostream& = std::cout) const;

  void probe(Server&, bool verbose); // Loads the dynamic library and runs it
  
  // Get functions
  std::string get_name() const {return name_;}
  std::string get_description() const {return description_;}
  std::vector<OPENSSL_VERSION> get_affected_versions() const {return affected_versions_;}
  int get_access_complexity() const {return access_complexity_;}
  int get_destructive() const {return destructive_;}
  std::string get_lib() const {return lib_;}
  int get_confidence() const {return confidence_;}
  std::string get_dependency() const {return dependency_;}

private:
  // These data members are equvalent to
  // the ones in the database described in database.h
  std::string name_;
  std::string description_;
  std::vector<OPENSSL_VERSION> affected_versions_;
  int access_complexity_;
  int destructive_;
  std::string lib_;
  int confidence_;
  std::string dependency_;
  
  std::string file2str(const char *filename);
};

std::string serialise_versions(const std::vector<OPENSSL_VERSION>&);
std::vector<OPENSSL_VERSION> unserialise_versions(const std::string&);

std::vector<std::string> json_in_dir(const char* = JSON_DIR);

#endif

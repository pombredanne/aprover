# Modified SO_RULE to not include ARCH (e.g. x86_64) in the output file name so that 
# my dynamic linking at runtime stays the same.
# Based on the SO_RULE found in the NaCl SDK

#
# SO Macro
#
# As well as building and installing a shared library this rule adds dependencies
# on the library's .stamp file in STAMPDIR.  However, the rule for creating the stamp
# file is part of LIB_RULE, so users of the DEPS system are currently required to
# use the LIB_RULE macro as well as the SO_RULE for each shared library.
#
# $1 = Target Name
# $2 = List of Sources
# $3 = List of LIBS
# $4 = List of DEPS
# $5 = Library Paths
# $6 = 1 => Don't add to NMF.
#
define MY_SO_LINKER_RULE
ifneq (,$(findstring x86_32,$(ARCHES)))
all: $(X86_32_OUTDIR)/lib$(1).so
$(X86_32_OUTDIR)/lib$(1).so: $(foreach src,$(2),$(call SRC_TO_OBJ,$(src),_x86_32_pic)) $(foreach dep,$(4),$(STAMPDIR)/$(dep).stamp)
	$(MKDIR) -p $$(dir $$@)
	$(call LOG,LINK,$$@,$(X86_32_LINK) -o $$@ $$(filter %.o,$$^) $(LDFLAGS_SHARED) -m32 $(NACL_LDFLAGS) $(X86_32_LDFLAGS) $(LDFLAGS) $(foreach path,$(5),-L$(path)/$(TOOLCHAIN)_x86_32/$(CONFIG)) $(foreach lib,$(3),-l$(lib)))
	$(call LOG,VALIDATE,$$@,$(NCVAL) $$@)

$(STAMPDIR)/$(1).stamp: $(LIBDIR)/$(TOOLCHAIN)_x86_32/$(CONFIG)/lib$(1).so
install: $(LIBDIR)/$(TOOLCHAIN)_x86_32/$(CONFIG)/lib$(1).so
$(LIBDIR)/$(TOOLCHAIN)_x86_32/$(CONFIG)/lib$(1).so: $(X86_32_OUTDIR)/lib$(1).so
	$(MKDIR) -p $$(dir $$@)
	$(call LOG,CP  ,$$@,$(OSHELPERS) cp $$^ $$@)
ifneq ($(6),1)
GLIBC_SO_LIST += $(X86_32_OUTDIR)/lib$(1).so
GLIBC_REMAP += -n lib$(1).so,lib$(1).so
endif
endif

ifneq (,$(findstring x86_64,$(ARCHES)))
#all: $(X86_64_OUTDIR)/lib$(1)_x86_64.so
all: $(X86_64_OUTDIR)/lib$(1).so # MY MOD
#$(X86_64_OUTDIR)/lib$(1)_x86_64.so: $(foreach src,$(2),$(call SRC_TO_OBJ,$(src),_x86_64_pic)) $(foreach dep,$(4),$(STAMPDIR)/$(dep).stamp)
$(X86_64_OUTDIR)/lib$(1).so: $(foreach src,$(2),$(call SRC_TO_OBJ,$(src),_x86_64_pic)) $(foreach dep,$(4),$(STAMPDIR)/$(dep).stamp) # MY MOD
	$(MKDIR) -p $$(dir $$@)
	$(call LOG,LINK,$$@,$(X86_32_LINK) -o $$@ $$(filter %.o,$$^) $(LDFLAGS_SHARED) -m64 $(NACL_LDFLAGS) $(X86_64_LDFLAGS) $(LDFLAGS) $(foreach path,$(5),-L$(path)/$(TOOLCHAIN)_x86_64/$(CONFIG)) $(foreach lib,$(3),-l$(lib)))
	$(call LOG,VALIDATE,$$@,$(NCVAL) $$@)

$(STAMPDIR)/$(1).stamp: $(LIBDIR)/$(TOOLCHAIN)_x86_64/$(CONFIG)/lib$(1).so
install: $(LIBDIR)/$(TOOLCHAIN)_x86_64/$(CONFIG)/lib$(1).so
#$(LIBDIR)/$(TOOLCHAIN)_x86_64/$(CONFIG)/lib$(1).so: $(X86_64_OUTDIR)/lib$(1)_x86_64.so 
$(LIBDIR)/$(TOOLCHAIN)_x86_64/$(CONFIG)/lib$(1).so: $(X86_64_OUTDIR)/lib$(1).so # MY MOD
	$(MKDIR) -p $$(dir $$@)
	$(call LOG,CP  ,$$@,$(OSHELPERS) cp $$^ $$@)
ifneq ($(6),1)
#GLIBC_SO_LIST += $(X86_64_OUTDIR)/lib$(1)_x86_64.so
GLIBC_SO_LIST += $(X86_64_OUTDIR)/lib$(1).so # MY MOD
#GLIBC_REMAP += -n lib$(1)_x86_64.so,lib$(1).so
GLIBC_REMAP += -n lib$(1).so,lib$(1).so # MY MOD
endif
endif

ifneq (,$(findstring arm,$(ARCHES)))
all: $(ARM_OUTDIR)/lib$(1).so
$(ARM_OUTDIR)/lib$(1).so: $(foreach src,$(2),$(call SRC_TO_OBJ,$(src),_arm_pic)) $(foreach dep,$(4),$(STAMPDIR)/$(dep).stamp)
	$(MKDIR) -p $$(dir $$@)
	$(call LOG,LINK,$$@,$(ARM_LINK) -o $$@ $$(filter %.o,$$^) $(LDFLAGS_SHARED) -marm $(NACL_LDFLAGS) $(ARM_LDFLAGS) $(LDFLAGS) $(foreach path,$(5),-L$(path)/$(TOOLCHAIN)_arm/$(CONFIG)) $(foreach lib,$(3),-l$(lib)))
	$(call LOG,VALIDATE,$$@,$(NCVAL) $$@)

$(STAMPDIR)/$(1).stamp: $(LIBDIR)/$(TOOLCHAIN)_arm/$(CONFIG)/lib$(1).so
install: $(LIBDIR)/$(TOOLCHAIN)_arm/$(CONFIG)/lib$(1).so
$(LIBDIR)/$(TOOLCHAIN)_arm/$(CONFIG)/lib$(1).so: $(ARM_OUTDIR)/lib$(1).so
	$(MKDIR) -p $$(dir $$@)
	$(call LOG,CP  ,$$@,$(OSHELPERS) cp $$^ $$@)
ifneq ($(6),1)
GLIBC_SO_LIST += $(ARM_OUTDIR)/lib$(1).so
GLIBC_REMAP += -n lib$(1).so,lib$(1).so
endif
endif
endef

#
# $1 = Target Name
# $2 = List of Sources
# $3 = List of LIBS
# $4 = List of DEPS
# $5 = 1 => Don't add to NMF.
#
define MY_SO_RULE
$(call MY_SO_LINKER_RULE,$(1),$(2),$(filter-out pthread,$(3)),$(4),$(LIB_PATHS),$(5))
endef
